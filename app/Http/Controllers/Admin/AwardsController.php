<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Award;

class AwardsController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/awards/');
        $file ->move($dir , $file_name);
        $name = '/images/awards/'.$file_name;
        return response()->json($name);
    }

    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $pages = Award::find($key);
                    $pages->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $pages = Award::orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($pages);
        }
    }
    public function create(Request $request)
    {

        $rules = [
            'title' => 'required|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $page = new Award;
        $page->title = $request->input('title');
        $page->order = $request->input('order');
        $page->file = $request->input('file');

        $page->save();

        return response()->json(['messages' => 'Product has been created']);

    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $page = Award::find($id);
            $page->title = $request->input('title');
            $page->order = $request->input('order');
            $page->file = $request->input('file');

            $page->save();

            return response()->json(['messages' => 'Product has been created']);
        } else {

            $page = Award::find($id);
            return response()->json($page);

        }
    }


    public function delete($id)
    {
        $page = Award::find($id);
        $page->delete();
    }
}
