<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Category;

class CategoriesController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/categories/');
        $file ->move($dir , $file_name);
        $name = '/images/categories/'.$file_name;
        return response()->json($name);
    }
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $pages = Category::find($key);
                    $pages->translations()->delete();
                    $pages->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $pages = Category::where('category_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($pages);
        }
    }
    public function create(Request $request)
    {

        $rules = [
            'title' => 'required|max:255',
            'lang' => 'required|max:3',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $page = new Category;
        $page->title = $request->input('title');
        $page->lang = $request->input('lang');
        $page->full = $request->input('full');
        $page->short = $request->input('short');
        $page->image = $request->input('image');

        $page->save();

        return response()->json(['messages' => 'Product has been created']);

    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'lang' => 'required|max:3',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $page = Category::find($id);
            $page->title = $request->input('title');
            $page->lang = $request->input('lang');
            $page->full = $request->input('full');
            $page->short = $request->input('short');
            $page->image = $request->input('image');

            $page->save();

            return response()->json(['messages' => 'Product has been created']);
        } else {

            $page = Category::find($id);
            return response()->json($page);

        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Category::where('category_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
                'lang' => 'required|max:3',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $page = Category::find($id);

            if (!$trans){
                $translation = new Category;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->full = $request->input('full');
                $translation->short = $request->input('short');
                $translation->image = $page->image;
                $page->translations()->save($translation);
            } else {
                $translation = $trans;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->full = $request->input('full');
                $translation->short = $request->input('short');
                $translation->image = $page->image;
                $translation->save();
            }

            return response()->json(['messages' => 'Product has been created']);
        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Category::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
