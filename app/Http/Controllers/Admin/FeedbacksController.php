<?php

namespace App\Http\Controllers\Admin;

use App\Models\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;

class FeedbacksController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/feedbacks/');
        $file ->move($dir , $file_name);
        $name = '/images/feedbacks/'.$file_name;
        return response()->json($name);
    }

    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Feedback::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $teachers = Feedback::where('feedback_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($teachers);
        }
    }
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'full' => 'required|string',
            'image' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $service = new Feedback;
        $service->title = $request->input('title');
        $service->lang = $request->input('lang');
        $service->full = $request->input('full');
        $service->image = $request->input('image');
        $service->save();


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'full' => 'required|string',
                'image' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $teacher = Feedback::find($id);
            $teacher->title = $request->input('title');
            $teacher->lang = $request->input('lang');
            $teacher->full = $request->input('full');
            $teacher->image = $request->input('image');
            $teacher->save();


            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $teacher = Feedback::find($id);
            return response()->json($teacher);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Feedback::where('feedback_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
                'full' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = Feedback::find($id);
            if (!$trans){
                $translation = new Feedback;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->full = $request->input('full');
                $translation->image = $page->image;
                $page->translations()->save($translation);
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->full = $request->input('full');
                $trans->image = $page->image;
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Feedback::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
