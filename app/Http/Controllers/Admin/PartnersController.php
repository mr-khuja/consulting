<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Partner;

class PartnersController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/partners/');
        $file ->move($dir , $file_name);
        $name = '/images/partners/'.$file_name;
        return response()->json($name);
    }

    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $pages = Partner::find($key);
                    $pages->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $pages = Partner::orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($pages);
        }
    }
    public function create(Request $request)
    {

        $rules = [
            'title' => 'required|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $page = new Partner;
        $page->title = $request->input('title');
        $page->order = $request->input('order');
        $page->image = $request->input('image');

        $page->save();

        return response()->json(['messages' => 'Product has been created']);

    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $page = Partner::find($id);
            $page->title = $request->input('title');
            $page->order = $request->input('order');
            $page->image = $request->input('image');

            $page->save();

            return response()->json(['messages' => 'Product has been created']);
        } else {

            $page = Partner::find($id);
            return response()->json($page);

        }
    }


    public function delete($id)
    {
        $page = Partner::find($id);
        $page->delete();
    }
}
