<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;

class ProductsController extends Controller
{
    public function order(Request $request){
        $order = $request->all();
        foreach($order as $key => $item){
            $menu = Product::find($item);
            $menu->order = $key;
            $menu->save();
        }
    }
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/products/');
        $file ->move($dir , $file_name);
        $name = '/images/products/'.$file_name;
        return response()->json($name);
    }

    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $pages = Product::find($key);
                    $pages->translations()->delete();
                    $pages->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $pages = Product::where('product_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($pages);
        }
    }
    public function create(Request $request)
    {

        $rules = [
            'title' => 'required|max:255',
            'lang' => 'required|max:3',
            'full' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $page = new Product;
        $page->title = $request->input('title');
        $page->lang = $request->input('lang');
        $page->short = $request->input('short');
        $page->full = $request->input('full');
        $page->image = $request->input('image');
        $page->order = $request->input('order');
        $page->baner = $request->input('baner');
        $page->category_id = $request->input('category_id');

        $page->save();

        return response()->json(['messages' => 'Product has been created']);

    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'full' => 'required|string',
                'lang' => 'required|max:3',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $page = Product::find($id);
            $page->title = $request->input('title');
            $page->lang = $request->input('lang');
            $page->short = $request->input('short');
            $page->full = $request->input('full');
            $page->image = $request->input('image');
            $page->order = $request->input('order');
            $page->baner = $request->input('baner');
            $page->category_id = $request->input('category_id');

            $page->save();

            return response()->json(['messages' => 'Product has been created']);
        } else {

            $page = Product::find($id);
            return response()->json($page);

        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Product::where('product_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
                'full' => 'required|string',
                'lang' => 'required|max:3',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $page = Product::find($id);

            if (!$trans){
                $translation = new Product;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->short = $request->input('short');
                $translation->full = $request->input('full');
                $translation->order = $page->order;
                $translation->image = $page->image;
                $translation->baner = $page->baner;
                $translation->category_id = $page->category_id;
                $page->translations()->save($translation);
            } else {
                $translation = $trans;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->short = $request->input('short');
                $translation->full = $request->input('full');
                $translation->order = $page->order;
                $translation->image = $page->image;
                $translation->baner = $page->baner;
                $translation->category_id = $page->category_id;
                $translation->save();
            }

            return response()->json(['messages' => 'Product has been created']);
        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Product::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
