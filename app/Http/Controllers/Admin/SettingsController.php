<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use Illuminate\Support\Facades\Auth;
use DB;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Page;
use App\Models\Feedback;
use App\Models\News;
use App\Models\Award;
use App\Models\Product;
use App\Models\Partner;

class SettingsController extends Controller
{
    public function settings(Request $request)
    {
        if ($request->isMethod('post')) {
            if (DB::table('settings')->where('id', 1)->exists()) {
                DB::table('settings')->where('id', 1)->update($request->all());
            } else {
                DB::table('settings')->where('id', 1)->insert($request->all());
            }

            return response()->json(['messages' => 'Product has been created']);
        } else {
            $settings = DB::table('settings')->first();
            return response()->json($settings);
        }
    }

    public function upload(Request $request)
    {
        $type = $request->type;
        $image = $request->file('image');
        $image_name = $type . '-' . time() . '-' . rand(0, 999999) . '.' . $image->getClientOriginalExtension();
        $dir = public_path('images/' . $type . '/');
        $image->move($dir, $image_name);
        $name = '/images/' . $type . '/' . $image_name;
        return response()->json($name);
    }

    public function profile(Request $request)
    {

        $user = User::find(1);

        if ($request->isMethod('post')) {

            $rules = [
                'name' => 'required|max:255|unique:users,name,1',
                'email' => 'required|email|unique:users,email,1',
                'image' => 'required|max:255',
                'password' => 'confirmed|min:8',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->image = $request->input('image');
            if ($request->filled('password')) {
                $hash = Hash::make($request->input('password'));
                $user->password = $hash;
            }
            $user->save();
            return response()->json(['messages' => 'Product has been created']);
        } else {
            return response()->json($user);
        }
    }

    public function social(Request $request)
    {
        if ($request->isMethod('post')) {
            if (DB::table('settings')->where('id', 1)->exists()) {
                DB::table('settings')->where('id', 1)->update($request->all());
            } else {
                DB::table('settings')->where('id', 1)->insert($request->all());
            }

            return response()->json(['messages' => 'Product has been created']);
        } else {
            $settings = DB::table('settings')->first();
            return response()->json($settings);
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('home');
    }

    public function counts()
    {
        $pages = Page::count();
        $products = Product::count();
        $feedbacks = Feedback::count();
        $news = News::count();
        $awards = Award::count();
        $partners = Partner::count();

        $data = [
            'pages' => $pages,
            'products' => $products,
            'feedbacks' => $feedbacks,
            'news' => $news,
            'awards' => $awards,
            'partners' => $partners,
        ];

        return response()->json($data);
    }

    public function date($data){
        foreach ($data as $item) {
            $item->day = date('d', strtotime($item->created_at));
            $item->month = date('F', strtotime($item->created_at));
        }
        return $data;
    }

    public function latest()
    {
        $products = $this->date(Product::latest()->limit(5)->get());
        $feedbacks = $this->date(Feedback::latest()->limit(5)->get());
        $news = $this->date(News::latest()->limit(5)->get());

        $data = [
            'products' => $products,
            'feedbacks' => $feedbacks,
            'news' => $news,
        ];

        return response()->json($data);
    }
}
