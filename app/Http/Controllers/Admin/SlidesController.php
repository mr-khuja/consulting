<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Slide;

class SlidesController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/slides/');
        $file ->move($dir , $file_name);
        $name = '/images/slides/'.$file_name;
        return response()->json($name);
    }
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $pages = Slide::find($key);
                    $pages->translations()->delete();
                    $pages->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $pages = Slide::where('slide_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($pages);
        }
    }
    public function create(Request $request)
    {

        $rules = [
            'title' => 'required|max:255',
            'lang' => 'required|max:3',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $page = new Slide;
        $page->title = $request->input('title');
        $page->lang = $request->input('lang');
        $page->desc = $request->input('desc');
        $page->link = $request->input('link');
        $page->image = $request->input('image');

        $page->save();

        return response()->json(['messages' => 'Product has been created']);

    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'lang' => 'required|max:3',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $page = Slide::find($id);
            $page->title = $request->input('title');
            $page->lang = $request->input('lang');
            $page->desc = $request->input('desc');
            $page->link = $request->input('link');
            $page->image = $request->input('image');

            $page->save();

            return response()->json(['messages' => 'Product has been created']);
        } else {

            $page = Slide::find($id);
            return response()->json($page);

        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Slide::where('slide_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
                'lang' => 'required|max:3',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $page = Slide::find($id);

            if (!$trans){
                $translation = new Slide;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->desc = $request->input('desc');
                $translation->link = $request->input('link');
                $translation->image = $page->image;
                $page->translations()->save($translation);
            } else {
                $translation = $trans;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->desc = $request->input('desc');
                $translation->link = $request->input('link');
                $translation->image = $page->image;
                $translation->save();
            }

            return response()->json(['messages' => 'Product has been created']);
        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Slide::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
