<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Vacancy;

class VacanciesController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/vacancies/');
        $file ->move($dir , $file_name);
        $name = '/images/vacancies/'.$file_name;
        return response()->json($name);
    }

    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $pages = Vacancy::find($key);
                    $pages->translations()->delete();
                    $pages->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $pages = Vacancy::where('vacancy_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($pages);
        }
    }
    public function create(Request $request)
    {

        $rules = [
            'title' => 'required|max:255',
            'lang' => 'required|max:3',
            'body' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $page = new Vacancy;
        $page->title = $request->input('title');
        $page->lang = $request->input('lang');
        $page->period = $request->input('period');
        $page->body = $request->input('body');
        $page->team = $request->input('team');

        $page->save();

        return response()->json(['messages' => 'Product has been created']);

    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'body' => 'required|string',
                'lang' => 'required|max:3',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $page = Vacancy::find($id);
            $page->title = $request->input('title');
            $page->body = $request->input('body');
            $page->team = $request->input('team');
            $page->lang = $request->input('lang');
            $page->period = $request->input('period');

            $page->save();

            return response()->json(['messages' => 'Product has been created']);
        } else {

            $page = Vacancy::find($id);
            return response()->json($page);

        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Vacancy::where('vacancy_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
                'body' => 'required|string',
                'lang' => 'required|max:3',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $page = Vacancy::find($id);

            if (!$trans){
                $translation = new Vacancy;
                $translation->title = $request->input('title');
                $translation->body = $request->input('body');
                $translation->team = $request->input('team');
                $translation->lang = $request->input('lang');
                $translation->period = $page->period;
                $page->translations()->save($translation);
            } else {
                $translation = $trans;
                $translation->title = $request->input('title');
                $translation->body = $request->input('body');
                $translation->team = $request->input('team');
                $translation->lang = $request->input('lang');
                $translation->period = $page->period;
                $translation->save();
            }

            return response()->json(['messages' => 'Product has been created']);
        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Vacancy::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
