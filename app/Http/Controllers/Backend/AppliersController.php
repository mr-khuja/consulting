<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Course;
use App\Models\Applier;
use App\Models\Test;
use App\Models\Question;
use App\Models\Answer;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProfileConfirm;
use DB;

class AppliersController extends Controller
{
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Test::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            if ($request->has('type')){
                $appliers = Applier::orderBy($request->currentSort, $request->currentSortDir)->where('test_result', '<>', 0)->with('group')->get();
            } else {
                $appliers = Applier::orderBy($request->currentSort, $request->currentSortDir)->where('test_result', 0)->with('group')->get();
            }

            return response()->json($appliers);
        }
    }
    public function results(Request $request)
    {
        foreach ($request->all() as $key => $item) {
            $applier = Applier::find($key);
            $applier->test_result = $item;
            if ($item > 30){
                $applier->state = 2;
            } else {
                $applier->state = 3;
            }
            $applier->save();
        }
//        return response()->json($request->all());
    }
    public function editm(Request $request){
//        return response()->json(dd($request->all()));
        foreach ($request->check as $key => $value){
            if ($value == 'true'){
                $applier = Applier::find($key);
//                return response()->json($request->multiple);
                if (isset($request->multiple['test_result'])){
                    $applier->test_result = $request->multiple['test_result'];
                }
                if (isset($request->multiple['group_id'])){
                    $applier->group_id = $request->multiple['group_id'];
                }
                if (isset($request->multiple['state'])){
                    $applier->state = $request->multiple['state'];
                }
                $applier->save();
            }
        }
    }
    public function edit(Request $request, $id){
        $applier = Applier::find($id);
        $applier->test_result = $request->test_result;
        $applier->group_id = $request->group_id;
        $applier->state = $request->state;
        $applier->save();
    }

    public function apply(Request $request){

        foreach ($request->all() as $key => $value){
            if ($value == 'true'){
                $applier = Applier::find($key);
                if($applier->state < 4) {
                    $applier->state = 4;
                    $applier->save();

                    $route = route('apply_profile', $applier->code);

                    $applier_mail = new \stdClass();
                    $applier_mail->link =  $route;
                    $applier_mail->sender = 'datasitesmtp@gmail.com';
                    $applier_mail->receiver = $applier->email;

                    Mail::to($applier->email)->send(new ProfileConfirm($applier_mail));
                }
            }
        }
    }

}
