<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Baner;

class BanersController extends Controller
{
    public function order(Request $request){
        $order = $request->all();
        foreach($order as $key => $item){
            $menu = Baner::find($item);
            $menu->order = $key;
            $menu->save();
        }
    }
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/baners/');
        $file ->move($dir , $file_name);
        $name = '/images/baners/'.$file_name;
        return response()->json($name);
    }

    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Baner::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $teachers = Baner::where('baner_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($teachers);
        }
    }
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'subtitle' => 'required|max:255',
            'body' => 'required|string',
            'image' => 'required|string',
            'order' => 'required|numeric',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $teacher = new Baner;
        $teacher->title = $request->input('title');
        $teacher->lang = $request->input('lang');
        $teacher->subtitle = $request->input('subtitle');
        $teacher->order = $request->input('order');
        $teacher->body = $request->input('body');
        $teacher->image = $request->input('image');
        $teacher->save();


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'subtitle' => 'required|max:255',
                'body' => 'required|string',
                'image' => 'required|string',
                'order' => 'required|numeric',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $teacher = Baner::find($id);
            $teacher->title = $request->input('title');
            $teacher->lang = $request->input('lang');
            $teacher->subtitle = $request->input('subtitle');
            $teacher->body = $request->input('body');
            $teacher->order = $request->input('order');
            $teacher->image = $request->input('image');
            $teacher->save();

            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $teacher = Baner::find($id);
            return response()->json($teacher);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Baner::where('baner_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
                'subtitle' => 'required|max:255',
                'body' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = Baner::find($id);
            if (!$trans){
                $translation = new Baner;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->subtitle = $request->input('subtitle');
                $translation->body = $request->input('body');
                $translation->image = $page->image;
                $translation->order = $page->order;
                $page->translations()->save($translation);
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->subtitle = $request->input('subtitle');
                $trans->body = $request->input('body');
                $trans->image = $page->image;
                $trans->order = $page->order;
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Baner::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
