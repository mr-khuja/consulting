<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Course;
use App\Models\Test;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Album;
use DB;

class CoursesController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/courses/');
        $file ->move($dir , $file_name);
        $name = '/images/courses/'.$file_name;
        return response()->json($name);
    }
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Test::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $courses = Course::where('course_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($courses);
        }
    }

    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'desc' => 'required|string',
            'subjects' => 'required',
            'start' => 'required|date_format:"U"',
            'finish' => 'required|date_format:"U"',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $isavtive = Course::where('isactive', 'true')->first();

        if ($isavtive && $request->input('isactive') == 'true') {
            return response()->json(['errors' => ['Уже существует активный курс']])->setStatusCode(422);
        }

        $course = new Course;
        $course->title = $request->input('title');
        $course->lang = $request->input('lang');
        $course->isactive = $request->input('isactive');
        $course->desc = $request->input('desc');
        $course->start = date('Y-m-d', $request->input('start')/1000);
        $course->finish = date('Y-m-d', $request->input('finish')/1000);
        $course->save();
        if ($request->test) {
            $test = Test::find($request->test);
            $test->course_id = $course->id;
            $test->save();
        }

        foreach ($request->input('album') as $image){
            $course->album()->create([
                'image' => $image
            ]);
        }
        foreach ($request->input('subjects') as $subject){
            DB::table('course_teachers')->insert([
                'course_id' => $course->id,
                'subject_id' => $subject
            ]);
        }

        return response()->json(['messages' => 'Node has been created']);

    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'desc' => 'required|string',
                'start' => 'required',
                'finish' => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $isavtive = Course::where('isactive', 'true')->where('id', '!=', $id)->first();

            if ($isavtive && $request->input('isactive') == 'true') {
                return response()->json(['errors' => ['Уже существует активный курс']])->setStatusCode(422);
            }

            $course = Course::find($id);
            $course->title = $request->input('title');
            $course->lang = $request->input('lang');
            $course->desc = $request->input('desc');


            $course->isactive = $request->input('isactive');

            if(is_numeric ($request->input('start'))){
                $course->start = date('Y-m-d', $request->input('start') / 1000);
            } else {
                $course->start = $request->input('start');
            }
            if(is_numeric ($request->input('finish'))){
                $course->finish = date('Y-m-d', $request->input('finish') / 1000);
            } else {
                $course->finish = $request->input('finish');
            }

            if ($request->tests) {
                $test = Test::find($request->tests);
                $test->course_id = $course->id;
                $test->save();
            }

            $course->save();
            DB::table('course_teachers')->where('course_id', $course->id)->delete();
            foreach ($request->input('subject') as $subject){
                DB::table('course_teachers')->insert([
                    'course_id' => $course->id,
                    'subject_id' => $subject
                ]);
            }

            foreach ($request->input('album') as $image){
                if (isset($image['id'])){
                    $album = Album::find($image['id']);
                    $album->image = $image['image'];
                    $album->save();
                } else {
                    $course->album()->create([
                        'image' => $image['image']
                    ]);
                }
            }


            return response()->json(['messages' => 'Node has been created']);
        } else {
            $course = Course::with(['test', 'subjects', 'album'])->find($id);
            $subjects = $course->subjects;
            $arr = [];
            foreach ($subjects as $subject) {
                $arr[] = $subject->id;
            }
            $course->subject = $arr;
//            dd($course);
            return response()->json($course);
        }
    }

    public function delete($id)
    {
        $data = Course::find($id);
        DB::table('course_teachers')->where('course_id', $data->id)->delete();
        $data->subjects()->delete();
        $test = Test::where('course_id', $data->id)->first();
        if ($test){
            $test->course_id = null;
            $test->save();
        }
        $data->translations()->delete();
        $data->album()->delete();
        $data->delete();
    }
}
