<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Email;
use DB;

class EmailsController extends Controller
{
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $data = Email::find($key);
                    DB::table('mail_group_list')->where('mail_id', $data->id)->delete();
                    $data->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $data = Email::with('mails')->orderBy($request->currentSort, $request->currentSortDir)->get();
            foreach ($data as $mail) {
                $groups = [];
                foreach ($mail->mails as $group) {
                    $groups[] = $group->id;
                }
                $mail->groups = $groups;
            }
            return response()->json($data);
        }
    }
    public function create(Request $request)
    {
        $rules = [
            'email' => 'required|email'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $data = new Email;
        $data->email = $request->input('email');
        $data->save();

        foreach ($request->groups as $group) {
            DB::table('mail_group_list')->insert([
                'mail_group_id' => $group,
                'mail_id' => $data->id
            ]);
        }

        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'email' => 'required|email'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $data = Email::find($id);
            $data->email = $request->input('email');
            $data->save();

            DB::table('mail_group_list')->where('mail_id', $data->id)->delete();

            foreach ($request->groups as $group) {
                DB::table('mail_group_list')->insert([
                    'mail_group_id' => $group,
                    'mail_id' => $data->id
                ]);
            }

            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $data = Email::with('mails')->find($id);
            $groups = [];
            foreach ($data->mails as $mail) {
                $groups[] = $mail->id;
            }
            $data->groups = $groups;
//            dd($data);
            return response()->json($data);
        }
    }

    public function delete($id)
    {
        $data = Email::find($id);
        DB::table('mail_group_list')->where('mail_id', $data->id)->delete();
        $data->delete();
    }
}
