<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\MailGroup;

class EmailsGroupsController extends Controller
{
    public function list(Request $request)
    {
        $data = MailGroup::orderBy($request->currentSort, $request->currentSortDir)->get();
        return response()->json($data);
    }
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $data = new MailGroup;
        $data->title = $request->input('title');
        $data->save();


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $data = MailGroup::find($id);
            $data->title = $request->input('title');
            $data->save();

            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $data = MailGroup::find($id);
            return response()->json($data);
        }
    }

    public function delete($id)
    {
        $data = MailGroup::find($id);
        $data->delete();
    }
}
