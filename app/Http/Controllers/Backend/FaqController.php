<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\FAQ;

class FaqController extends Controller
{
    public function order(Request $request){
        $order = $request->all();
        foreach($order as $key => $item){
            $menu = FAQ::find($item);
            $menu->order = $key;
            $menu->save();
        }
    }
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $page = FAQ::find($key);
                    $page->translations()->delete();
                    $page->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $pages = FAQ::where('faq_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($pages);
        }
    }
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'body' => 'required|string',
            'order' => 'required|numeric',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $page = new FAQ;
        $page->title = $request->input('title');
        $page->lang = $request->input('lang');
        $page->body = $request->input('body');
        $page->order = $request->input('order');
        $page->save();


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'body' => 'required|string',
                'order' => 'required|numeric',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $page = FAQ::find($id);
            $page->title = $request->input('title');
            $page->lang = $request->input('lang');
            $page->body = $request->input('body');
            $page->order = $request->input('order');
            $page->save();

            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $page = FAQ::find($id);
            return response()->json($page);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = FAQ::where('faq_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = FAQ::find($id);
            if (!$trans){
                $translation = new FAQ;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->body = $request->input('body');
                $translation->order = $page->order;
                $page->translations()->save($translation);
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->body = $request->input('body');
                $trans->order = $page->order;
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = FAQ::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
