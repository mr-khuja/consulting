<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Feedback;

class FeedbacksController extends Controller
{
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Feedback::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $teachers = Feedback::orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($teachers);
        }
    }
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'body' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $teacher = new Feedback;
        $teacher->title = $request->input('title');
        $teacher->body = $request->input('body');
        $teacher->save();


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'body' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $teacher = Feedback::find($id);
            $teacher->title = $request->input('title');
            $teacher->body = $request->input('body');
            $teacher->save();

            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $teacher = Feedback::find($id);
            return response()->json($teacher);
        }
    }


    public function delete($id)
    {
        $page = Feedback::find($id);
        $page->delete();
    }
}
