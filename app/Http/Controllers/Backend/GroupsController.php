<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Course;
use App\Models\Test;
use App\Models\Question;
use App\Models\Group;

class GroupsController extends Controller
{
    public function list(Request $request)
    {

        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Test::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $course = Group::where('group_id', null)->with('course')->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($course);
        }
    }

    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'course_id' => 'required|exists:course,id',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $course = new Group;
        $course->title = $request->input('title');
        $course->lang = $request->input('lang');
        $course->course_id = $request->input('course_id');
        $course->save();

        return response()->json(['messages' => 'Node has been created']);

    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'course_id' => 'required|exists:course,id',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $course = Group::find($id);
            $course->title = $request->input('title');
            $course->lang = $request->input('lang');
            $course->course_id = $request->input('course_id');
            $course->save();

            return response()->json(['messages' => 'Node has been created']);
        } else {
            $course = Group::find($id);
            return response()->json($course);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Group::where('group_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = Group::find($id);
            if (!$trans){
                $translation = new Group;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->course_id = $page->course_id;
                $page->translations()->save($translation);
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->course_id = $page->course_id;
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {
            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Group::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
