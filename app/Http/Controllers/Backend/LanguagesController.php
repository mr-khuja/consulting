<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Language;

class LanguagesController extends Controller
{
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $data = Language::find($key);
                    $data->translations()->delete();
                    $data->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $data = Language::where('language_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($data);
        }
    }
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $data = new Language;
        $data->title = $request->input('title');
        $data->lang = $request->input('lang');
        if($request->has('isother')){
            $data->isother = $request->input('isother');
        }
        $data->save();


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $data = Language::find($id);
            $data->title = $request->input('title');
            $data->lang = $request->input('lang');
            if($request->has('isother')){
                $data->isother = $request->input('isother');
            }
            $data->save();

            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $data = Language::find($id);
            return response()->json($data);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Language::where('language_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = Language::find($id);
            if (!$trans){
                $translation = new Language;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->isother = $page->isother;
                $page->translations()->save($translation);
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->isother = $page->isother;
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $data = Language::find($id);
        $data->translations()->delete();
        $data->delete();
    }
}
