<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Course;
use App\Models\Test;
use App\Models\Material;
use App\Models\Answer;

class MaterialsController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/materials/');
        $file ->move($dir , $file_name);
        $name = 'images/materials/'.$file_name;
        return response()->json($name);
    }

    public function list(Request $request)
    {

        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Test::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $subjects = Material::where('material_id', null)->with('subject')->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($subjects);
        }
    }

    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'file' => 'required',
            'ext' => 'required',
            'subject_id' => 'required|exists:subject,id',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $material = new Material;
        $material->title = $request->input('title');
//        $material->lang = $request->input('lang');
        $material->file = $request->input('file');
        $material->subject_id = $request->input('subject_id');
        $material->ext = $request->input('ext');
        $material->save();

        return response()->json(['messages' => 'Node has been created']);

    }

    public function edit(Request $request, $id)
    {
        $material = Material::find($id);
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'file' => 'required',
                'ext' => 'required',
                'subject_id' => 'required|exists:subject,id',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $material->title = $request->input('title');
//            $material->lang = $request->input('lang');
            $material->file = $request->input('file');
            $material->subject_id = $request->input('subject_id');
            $material->ext = $request->input('ext');
            $material->save();
            return response()->json(['messages' => 'Node has been created']);
        } else {
            return response()->json($material);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Test::where('test_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
                'questions.*' => 'required',
                'questions.*.title' => 'max:255',
                'questions.*.answers.*' => 'required',
                'questions.*.answers.*.title' => 'max:255',
                'questions.*.answers.*.istrue' => 'boolean',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = Test::find($id);
            if (!$trans){
                $translation = new Test;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $page->translations()->save($translation);
                foreach ($request->input('questions') as $question){
                    $questionn = $translation->questions()->create([
                        'title' => $question['title'],
                        'lang' => $request->input('lang')
                    ]);
                    foreach ($question['answers'] as $answer){
                        $questionn->answers()->create([
                            'title' => $answer['title'],
                            'lang' => $request->input('lang'),
                            'istrue' => $answer['istrue']
                        ]);
                    }
                }
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->save();
                $aa = $trans->questions;
                foreach ($aa as $qq){
                    $qq->answers()->delete();
                }
                $trans->questions()->delete();
                foreach ($request->input('questions') as $question){
                    $questionn = $trans->questions()->create([
                        'title' => $question['title'],
                        'lang' => $request->input('lang')
                    ]);
                    foreach ($question['answers'] as $answer){
                        $questionn->answers()->create([
                            'title' => $answer['title'],
                            'lang' => $request->input('lang'),
                            'istrue' => $answer['istrue']
                        ]);
                    }
                }
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {
            if (!$trans){
                $trans = Test::with('questions.answers')->find($id);
            }
            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Test::find($id);
        $trans = $page->translations;
        $aa = $trans->questions;
        foreach ($aa as $qq){
            $qq->answers()->delete();
        }
        $bb = $page->questions;
        foreach ($bb as $qq){
            $qq->answers()->delete();
        }
        $trans->questions()->delete();
        $page->translations()->delete();
        $page->delete();
    }

    public function applier($id){
        $applier_test = DB::table('applier_test')->where('applier_id', $id)->first();
        $test = Test::with('questions.answers')->find($applier_test->test_id);
        $answers = DB::table('applier_answer')->where('applier_test_id', $applier_test->id)->get();
        $data = new \stdClass();
        $data->test = $test;
        $data->answer = $answers;
//        dd($data);
        return response()->json($data);
//        dd($applier_test);
    }
}
