<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Menu;

class MenuController extends Controller
{

    public function weight(Request $request){
        $weight = $request->all();
        foreach($weight as $item){
            $menu = Menu::find($item['id']);
            $menu->order = $item['order'];
            if($item['parent_id'] != 0){
                $menu->parent_id = $item['parent_id'];
            } else {
                $menu->parent_id = null;
            }
            $menu->save();
        }
    }
    public function order(Request $request){
        $order = $request->all();
        foreach($order as $key => $item){
            $menu = Menu::find($item);
            $menu->order = $key;
            $menu->save();
        }
    }
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $page = Menu::find($key);
                    $page->translations()->delete();
                    $page->delete();
                }
            }
            return response()->json($request->all());
        } else {
            if($request->wch == 'true'){
                $pages = Menu::with('children.children')->where('menu_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            } else {
                $pages = Menu::with('children.children')->where('parent_id', null)->where('menu_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            }
//            $pages = Menu::with('children')->where('menu_id', null)->get();
//            foreach($pages as $menu){
//                $menu->children = [];
//            }
            return response()->json($pages);
        }
    }
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'path' => 'required|max:255',
            'order' => 'required|numeric',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $page = new Menu;
        $page->title = $request->input('title');
        $page->lang = $request->input('lang');
        $page->path = $request->input('path');
        $page->order = $request->input('order');
        $page->save();


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'path' => 'required|max:255',
                'order' => 'required|numeric',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $page = Menu::find($id);
            $page->title = $request->input('title');
            $page->lang = $request->input('lang');
            $page->path = $request->input('path');
            $page->order = $request->input('order');
            $page->save();

            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $page = Menu::find($id);
            return response()->json($page);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Menu::where('menu_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = Menu::find($id);
            if (!$trans){
                $translation = new Menu;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->path = $page->path;
                $translation->order = $page->order;
                $page->translations()->save($translation);
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->path = $page->path;
                $trans->order = $page->order;
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Menu::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
