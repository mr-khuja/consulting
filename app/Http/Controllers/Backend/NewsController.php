<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\News;

class NewsController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/news/');
        $file ->move($dir , $file_name);
        $name = '/images/news/'.$file_name;
        return response()->json($name);
    }

    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = News::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $teachers = News::where('news_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($teachers);
        }
    }
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'short' => 'required|max:255',
            'body' => 'required|string',
            'image' => 'required|string',
            'category' => 'required|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $teacher = new News;
        $teacher->title = $request->input('title');
        $teacher->lang = $request->input('lang');
        $teacher->category = $request->input('category');
        $teacher->short = $request->input('short');
        $teacher->body = $request->input('body');
        $teacher->image = $request->input('image');
        $teacher->save();


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'short' => 'required|max:255',
                'body' => 'required|string',
                'image' => 'required|string',
                'category' => 'required|max:255',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $teacher = News::find($id);
            $teacher->title = $request->input('title');
            $teacher->lang = $request->input('lang');
            $teacher->short = $request->input('short');
            $teacher->body = $request->input('body');
            $teacher->category = $request->input('category');
            $teacher->image = $request->input('image');
            $teacher->save();

            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $teacher = News::find($id);
            return response()->json($teacher);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = News::where('news_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
                'short' => 'required|max:255',
                'body' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = News::find($id);
            if (!$trans){
                $translation = new News;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->short = $request->input('short');
                $translation->body = $request->input('body');
                $translation->image = $page->image;
                $translation->category = $page->category;
                $page->translations()->save($translation);
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->short = $request->input('short');
                $trans->category = $page->category;
                $trans->body = $request->input('body');
                $trans->image = $page->image;
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = News::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
