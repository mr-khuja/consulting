<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\Page;
use Validator;

class PagesController extends Controller
{
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $pages = Page::find($key);
                    $pages->translations()->delete();
                    $pages->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $pages = Page::where('page_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($pages);
        }
    }
    public function create(Request $request)
    {

        $rules = [
            'title' => 'required|max:255',
            'lang' => 'required|max:3',
            'body' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
            '\\', '|', '/', '.', ',', '`', ' '
        ];
        $lat = [
            'a','b','v','g','d','e','yo','j','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sh','','i','y','e','yu','ya',
            'a','b','v','g','d','e','yo','j','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sh','','i','y','e','yu','ya',
            '-', '-', '-', '-', '-', '-', '-'
        ];

        $alias = substr(str_replace($cyr, $lat, $request->input('title')), 0, 200);

        $page = new Page;
        $page->title = $request->input('title');
        $page->lang = $request->input('lang');
        $page->short = $request->input('short');
        $page->tags = $request->input('tags');
        $page->alias = $alias;
        $page->body = $request->input('body');

        $page->save();

        return response()->json(['messages' => 'Product has been created']);

    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'body' => 'required|string',
                'lang' => 'required|max:3',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $cyr = [
                'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
                'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
                'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
                'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
                '\\', '|', '/', '.', ',', '`', ' '
            ];
            $lat = [
                'a','b','v','g','d','e','yo','j','z','i','y','k','l','m','n','o','p',
                'r','s','t','u','f','h','ts','ch','sh','sh','','i','y','e','yu','ya',
                'a','b','v','g','d','e','yo','j','z','i','y','k','l','m','n','o','p',
                'r','s','t','u','f','h','ts','ch','sh','sh','','i','y','e','yu','ya',
                '-', '-', '-', '-', '-', '-', '-'
            ];


            $alias = substr(str_replace($cyr, $lat, $request->input('title')), 0, 200);

            $page = Page::find($id);
            $page->tags = $request->input('tags');
            $page->short = $request->input('short');
            $page->title = $request->input('title');
            $page->alias = $alias;
            $page->body = $request->input('body');

            $page->save();

            return response()->json(['messages' => 'Product has been created']);
        } else {

            $page = Page::find($id);
            return response()->json($page);

        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Page::where('page_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
                'body' => 'required|string',
                'lang' => 'required|max:3',
            ];

            $validator = Validator::make($request->all(), $rules);

            $cyr = [
                'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
                'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
                'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
                'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
                '\\', '|', '/', '.', ',', '`', ' '
            ];
            $lat = [
                'a','b','v','g','d','e','yo','j','z','i','y','k','l','m','n','o','p',
                'r','s','t','u','f','h','ts','ch','sh','sh','','i','y','e','yu','ya',
                'a','b','v','g','d','e','yo','j','z','i','y','k','l','m','n','o','p',
                'r','s','t','u','f','h','ts','ch','sh','sh','','i','y','e','yu','ya',
                '-', '-', '-', '-', '-', '-', '-'
            ];


            $alias = substr(str_replace($cyr, $lat, $request->input('title')), 0, 200);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            if (!$trans){
                $page = Page::find($id);
                $translation = new Page;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->body = $request->input('body');
                $translation->tags = $request->input('tags');
                $translation->short = $request->input('short');
                $translation->alias = $alias;
                $page->translations()->save($translation);
            } else {
                $translation = $trans;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->body = $request->input('body');
                $translation->tags = $request->input('tags');
                $translation->short = $request->input('short');
                $translation->alias = $alias;
                $translation->save();
            }

            return response()->json(['messages' => 'Product has been created']);
        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Page::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
