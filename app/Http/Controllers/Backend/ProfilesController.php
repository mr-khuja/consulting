<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Applier;
use App\Models\Profile;
use App\Mail\ProfileConfirm;
use Illuminate\Support\Facades\Mail;

class ProfilesController extends Controller
{
    public function ed(Request $request, $id){
        $profile = Profile::find($id);
        $profile->points = $request->points;
        $profile->save();
    }

    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Test::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $profile = Profile::orderBy($request->currentSort, $request->currentSortDir)->with([
                'applier.group', 'country', 'source', 'education.type', 'language.lang', 'soft.sf'
            ])->get();
            return response()->json($profile);
        }
    }
    public function confirm($id){

        $profile = Profile::with('applier')->find($id);
        $applier = Applier::find($profile->applier->id);
        $applier->state = 5;
        $applier->save();
        $route = route('apply_registration', $profile->applier->code);
        $email = new \stdClass();
        $email->link =  $route;
        $email->sender = 'datasitesmtp@gmail.com';
        $email->receiver = $profile->work_email;

        Mail::to($profile->work_email)->send(new ProfileConfirm($email));
    }
}
