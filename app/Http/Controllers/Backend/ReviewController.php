<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Review;

class ReviewController extends Controller
{
    public function list(Request $request)
    {
        $data = Review::with(['user', 'subject'])->orderBy($request->currentSort, $request->currentSortDir)->get();
        return response()->json($data);
    }
}
