<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use Illuminate\Support\Facades\Auth;
use DB;
use App\User;


use App\Exports\AppliersExport;
use Maatwebsite\Excel\Facades\Excel;


class SettingsController extends Controller
{
    public function settings(Request $request){
        if ($request->isMethod('post')) {
            if (DB::table('settings')->where('id', 1)->exists()) {
                DB::table('settings')->where('id', 1)->update($request->all());
            } else {
                DB::table('settings')->where('id', 1)->insert($request->all());
            }

            return response()->json(['messages' => 'Product has been created']);
        } else {
            $settings = DB::table('settings')->first();
            return response()->json($settings);
        }
    }
    public function upload(Request $request){
        $type = $request->type;
        $image = $request->file('image');
        $image_name = $type.'-'.time().'-'.rand(0,999999).'.'.$image->getClientOriginalExtension();
        $dir = public_path('images/'.$type.'/');
        $image->move($dir , $image_name);
        $name = '/images/'.$type.'/'.$image_name;
        return response()->json($name);
    }
    public function profile(Request $request){

        $user = User::find(1);

        if ($request->isMethod('post')) {

            $rules = [
                'name' => 'required|max:255|unique:users,name,1',
                'email' => 'required|email|unique:users,email,1',
                'image' => 'required|max:255',
                'password' => 'confirmed|min:8',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->image = $request->input('image');
            if($request->filled('password')) {
                $hash = Hash::make($request->input('password'));
                $user->password = $hash;
            }
            $user->save();
            return response()->json(['messages' => 'Product has been created']);
        } else {
            return response()->json($user);
        }
    }
    public function social(Request $request){
        if ($request->isMethod('post')) {
            if (DB::table('settings')->where('id', 1)->exists()) {
                DB::table('settings')->where('id', 1)->update($request->all());
            } else {
                DB::table('settings')->where('id', 1)->insert($request->all());
            }

            return response()->json(['messages' => 'Product has been created']);
        } else {
            $settings = DB::table('settings')->first();
            return response()->json($settings);
        }
    }
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('home');
    }
    public function export(Request $request){
//        return response()->json($request->all());
        $start = rand(1, 100);
        $end = rand(1, 100);
        $name = '/storage/excel/'.$start.'-'.$end.'.xlsx';
        Excel::store(new AppliersExport($request->all()), $start.'-'.$end.'.xlsx', 'excel');
        return response()->json($name);
    }
}
