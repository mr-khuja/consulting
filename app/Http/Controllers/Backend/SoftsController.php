<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Soft;

class SoftsController extends Controller
{
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $data = Soft::find($key);
                    $data->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $data = Soft::orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($data);
        }
    }
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $data = new Soft;
        $data->title = $request->input('title');
        $data->save();


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $data = Soft::find($id);
            $data->title = $request->input('title');
            $data->save();

            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $data = Soft::find($id);
            return response()->json($data);
        }
    }

    public function delete($id)
    {
        $data = Soft::find($id);
        $data->delete();
    }
}
