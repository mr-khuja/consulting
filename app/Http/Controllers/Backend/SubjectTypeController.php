<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Subject_Type;

class SubjectTypeController extends Controller
{
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $data = Subject_Type::find($key);
                    $data->translations()->delete();
                    $data->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $data = Subject_Type::where('subject_type_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($data);
        }
    }
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $data = new Subject_Type;
        $data->title = $request->input('title');
        $data->lang = $request->input('lang');
        $data->save();


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $data = Subject_Type::find($id);
            $data->title = $request->input('title');
            $data->lang = $request->input('lang');
            $data->save();

            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $data = Subject_Type::find($id);
            return response()->json($data);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Subject_Type::where('subject_type_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = Subject_Type::find($id);
            if (!$trans){
                $translation = new Subject_type;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $page->translations()->save($translation);
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $data = Subject_Type::find($id);
        $data->translations()->delete();
        $data->delete();
    }
}
