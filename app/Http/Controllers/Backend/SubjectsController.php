<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use DB;
use App\Models\Subject;

class SubjectsController extends Controller
{
    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Test::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $subject = Subject::where('subject_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($subject);
        }
    }

    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'desc' => 'required|string',
            'teachers' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $subject = new Subject;
        $subject->title = $request->input('title');
        $subject->lang = $request->input('lang');
        $subject->desc = $request->input('desc');
        $subject->save();
        foreach ($request->teachers as $teacher) {
            DB::table('teacher_subjects')->insert([
                'teacher_id' => $teacher,
                'subject_id' => $subject->id
            ]);
        }


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'desc' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $subject = Subject::find($id);
            $subject->title = $request->input('title');
            $subject->lang = $request->input('lang');
            $subject->desc = $request->input('desc');
            DB::table('teacher_subjects')->where('subject_id', $subject->id)->delete();
            foreach ($request->teacher as $teacher) {
                DB::table('teacher_subjects')->insert([
                    'teacher_id' => $teacher,
                    'subject_id' => $subject->id
                ]);
            }

            $subject->save();

            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $subject = Subject::find($id);
            $teachers = $subject->teachers;
            $arr = [];
            foreach ($teachers as $teacher) {
                $arr[] = $teacher->id;
            }
            $subject->teacher = $arr;
            return response()->json($subject);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Subject::where('subject_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
                'desc' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = Subject::find($id);
            if (!$trans){
                $translation = new Teacher;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->desc = $request->input('desc');
                $page->translations()->save($translation);
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->desc = $request->input('desc');
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Subject::find($id);
        DB::table('teacher_subjects')->where('subject_id', $page->id)->delete();
        $page->translations()->delete();
        $page->delete();
    }
}
