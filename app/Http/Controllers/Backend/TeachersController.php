<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Teacher;
use App\Models\Teacher_social;

class TeachersController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $file_name = time().'-'.rand(0,999999).'.'.$file->getClientOriginalExtension();
        $dir = public_path('images/teachers/');
        $file ->move($dir , $file_name);
        $name = '/images/teachers/'.$file_name;
        return response()->json($name);
    }

    public function list(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Test::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $teachers = Teacher::where('teacher_id', null)->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($teachers);
        }
    }
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'desc' => 'required|string',
            'image' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $teacher = new Teacher;
        $teacher->title = $request->input('title');
        $teacher->lang = $request->input('lang');
        $teacher->desc = $request->input('desc');
        $teacher->image = $request->input('image');
        $teacher->save();

        foreach($request->socials as $sc){
            $social = new Teacher_social;
            $social->link = $sc['link'];
            $social->type = $sc['type'];
            $teacher->socials()->save($social);
        }


        return response()->json(['messages' => 'Node has been created']);

    }
    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
                'desc' => 'required|string',
                'image' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }


            $teacher = Teacher::find($id);
            $teacher->title = $request->input('title');
            $teacher->lang = $request->input('lang');
            $teacher->image = $request->input('image');
            $teacher->desc = $request->input('desc');
            $teacher->save();

            $teacher->socials()->delete();

            foreach($request->socials as $sc){
                $social = new Teacher_social;
                $social->link = $sc['link'];
                $social->type = $sc['type'];
                $teacher->socials()->save($social);
            }


            return response()->json(['messages' => 'Node has been updated']);
        } else {
            $teacher = Teacher::with('socials')->find($id);
            return response()->json($teacher);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Teacher::where('teacher_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
                'desc' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = Teacher::find($id);
            if (!$trans){
                $translation = new Teacher;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->desc = $request->input('desc');
                $translation->image = $page->image;
                $page->translations()->save($translation);
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->image = $page->image;
                $trans->desc = $request->input('desc');
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {

            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Teacher::find($id);
        $page->translations()->delete();
        $page->delete();
    }
}
