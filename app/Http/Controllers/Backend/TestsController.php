<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Validator;
use App\Models\Course;
use App\Models\Test;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Applier;
use DB;
use App\Mail\ApplierConfirm;
use Illuminate\Support\Facades\Mail;

class TestsController extends Controller
{
    public function list(Request $request)
    {

        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Test::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $tests = Test::with('course')->orderBy($request->currentSort, $request->currentSortDir)->get();
            return response()->json($tests);
        }
    }

    public function create(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
//            'start' => 'required|date_format:"U"',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
        }

        $test = new Test;
        $test->title = $request->input('title');
//        $test->lang = $request->input('lang');
        if ($request->course_id) {
            $test->course_id = $request->input('course_id');
        } else {
            $test->course_id = null;
        }

//        $test->start = date('Y-m-d', $request->input('start')/1000);
        $test->save();


        foreach($request->days as $day){
            DB::table('course_test')->insert(
                [
                    'date' => date('Y-m-d', $day['date']/1000),
                    'test_id' => $test->id,
                    'limit' => $day['limit'],
                    'mail' => $day['mail'],
                ]
            );
        }

        return response()->json(['messages' => 'Node has been created']);

    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255',
//                'start' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }

            $test = Test::find($id);
            $test->title = $request->input('title');
//            $test->lang = $request->input('lang');
            if ($request->course_id) {
                $test->course_id = $request->input('course_id');
            } else {
                $test->course_id = null;
            }

            $test->save();

            DB::table('course_test')->where('test_id', $test->id)->delete();

            foreach($request->days as $day){
                if(is_numeric ($day['date'])){
                    $date = date('Y-m-d', $day['date'] / 1000);
                } else {
                    $date = $day['date'];
                }
                DB::table('course_test')->insert(
                    [
                        'date' => $date,
                        'test_id' => $test->id,
                        'limit' => $day['limit'],
                        'mail' => $day['mail'],
                    ]
                );
            }

            return response()->json(['messages' => 'Node has been created']);
        } else {
            $test = Test::find($id);
            $days = DB::table('course_test')->where('test_id', $test->id)->get();
            $test->days = $days;
            return response()->json($test);
        }
    }

    public function translate(Request $request, $id, $lang)
    {

        $trans = Test::where('test_id', $id)->where('lang', $lang)->first();

        if ($request->isMethod('post')) {

            $rules = [
                'title' => 'required|max:255',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()])->setStatusCode(422);
            }
            $page = Test::find($id);
            if (!$trans){
                $translation = new Test;
                $translation->title = $request->input('title');
                $translation->lang = $request->input('lang');
                $translation->start = $page->start;
                $translation->course_id = $page->course_id;
                $page->translations()->save($translation);
            } else {
                $trans->title = $request->input('title');
                $trans->lang = $request->input('lang');
                $trans->start = $page->start;
                $trans->course_id = $page->course_id;
                $trans->save();
            }

            return response()->json(['messages' => 'Node has been updated']);

        } else {
            if (!$trans){
                $trans = Test::find($id);
            }
            return response()->json($trans);

        }
    }

    public function delete($id)
    {
        $page = Test::find($id);
        DB::table('course_test')->where('test_id', $page->id)->delete();
//        $page->translations()->delete();
        $page->delete();
    }

    public function applier($id){
        $applier_test = DB::table('applier_test')->where('applier_id', $id)->first();
        $test = Test::with('questions.answers')->find($applier_test->test_id);
        $answers = DB::table('applier_answer')->where('applier_test_id', $applier_test->id)->get();
        $data = new \stdClass();
        $data->test = $test;
        $data->answer = $answers;
//        dd($data);
        return response()->json($data);
//        dd($applier_test);
    }

    public function result(Request $request)
    {

        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value){
                if ($value == 'true'){
                    $project = Test::find($key);
                    $project->translations()->delete();
                    $project->delete();
                }
            }
            return response()->json($request->all());
        } else {
            $appliers = Applier::get();
            $data = [];
            foreach($appliers as $applier){
                $a_test = DB::table('applier_test')->where('applier_id', $applier->id)->first();
                if ($a_test){
                    $a_answers = DB::table('applier_answer')->where('applier_test_id', $a_test->id)->get();
                    $test = Test::find($a_test->test_id);
                    $true_a = 0;
                    $flase_a = 0;
                    foreach($a_answers as $a_answer){
                        $answer = Answer::find($a_answer->answer_id);
                        if ($answer->istrue == 1){
                            $true_a = $true_a + 1;
                        } else {
                            $flase_a = $flase_a + 1;
                        }
                    }
                    $result = [
                        'aid' => $applier->id,
                        'state' => $applier->state,
                        'tid' => $test->id,
                        'title' => $applier->name,
                        'test_id' => $test->id,
                        'test' => $test->title,
                        'true_a' => $true_a,
                        'false_a' => $flase_a,
                        'created_at' => $a_test->created_at
                    ];
                    array_push($data, $result);
                }
            }
//            dd($data);

            return response()->json($data);
        }
    }
    public function confirm($id){

        $profile = Applier::find($id);
        $route = route('apply_test', $profile->code);
        $email = new \stdClass();
        $email->link =  $route;
        $email->sender = 'datasitesmtp@gmail.com';
        $email->receiver = $profile->email;

        Mail::to($profile->email)->send(new ApplierConfirm($email));
    }
}
