<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Response;
use Validator;

class UsersController extends Controller
{
    public function list(Request $request)
    {
//        $users = User::orderBy($request->currentSort, $request->currentSortDir)->where('role', 'student')->with('applier.profile')->get();
        $users = User::orderBy($request->currentSort, $request->currentSortDir)->with([
            'applier.profile.country',
            'applier.profile.source',
            'applier.profile.payer',
            'applier.group'
        ])->get();
        return response()->json($users);
    }
}
