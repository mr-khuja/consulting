<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Slide;
use App\Models\Menu;
use App\Models\Page;
use App\Models\Feedback;
use App\Models\News;
use App\Models\Award;
use App\Models\Product;
use App\Models\Partner;
use App\Models\Vacancy;
use App\Models\Category;
use App\Mail\ContactEmail;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

    public function lang($id)
    {
        $lang = app()->getLocale();
        switch ($lang) {
            case 'ru' :
                return Page::find($id);
                break;
            case 'en' :
                $pg = Page::where('lang', 'en')->where('page_id', $id)->first();
                if ($pg){
                    return $pg;
                } else {
                    return Page::find($id);
                }
                break;
        }
    }

    public function __construct()
    {

    }

    public function getLocale(){
        $lang = app()->getLocale();
        $menu = Menu::where('lang', $lang)->where('parent_id', null)->with([
            'children' => function($query){
                $query->orderBy('order');
            }
        ])->orderBy('order')->get();
        View::share('menu', $menu);

        $ids = [28,30,32,34];
        $pages = [];
        foreach ($ids as $id){
            $page = $this->lang($id);
            $pages[] = $page;
        }
        View::share('submenu', $pages);

        $cats = Category::where('lang', $lang)->get();
        View::share('cats', $cats);

        $about = $this->lang(1);
        View::share('abt', $about);
        $news = DB::table('news')->where('lang', $lang)->limit(2)->get();
        View::share('news', $news);

    }

    public function home()
    {
        $this->getLocale();
        $lang = app()->getLocale();

        $baner = Slide::where('lang', $lang)->limit(5)->get();

        $news = News::where('lang', $lang)->limit(4)->get();

        $feedbacks = Feedback::where('lang', $lang)->limit(8)->get();

        $products = Product::where('lang', $lang)->get();

        $partners = Partner::get();

        $mission = $this->lang(4);
        $history = $this->lang(3);
        $about = $this->lang(1);

        return view('home')->with([
            'baner' => $baner,
            'news' => $news,
            'partners' => $partners,
            'feedbacks' => $feedbacks,
            'products' => $products,
            'about' => [
                'page' => $about,
                'mission' => $mission,
                'history' => $history
            ]
        ]);
    }

    public function news($id)
    {
        $this->getLocale();
        $new = News::where('id', $id)->first();
        return view('templates.news.inner')->with(['new' => $new]);
    }

    public function NewsPage()
    {
        $this->getLocale();
        $lang = app()->getLocale();
        $news = News::where('lang', $lang)->orderBy('created_at', 'desc')->paginate(9);
        return view('templates.news.page')->with(['news' => $news]);
    }

    public function page($id)
    {
        $this->getLocale();
        $pages = $this->lang($id);
        if($pages->id == 2 || $pages->page_id == 2){
            return view('contact')->with(['page' => $pages]);
        }
        if($pages->id == 8 || $pages->page_id == 8){
            return view('templates.pages.pages')->with(['ceo'=> true, 'page' => $pages]);
        }
        if($pages->id == 6 || $pages->page_id == 6){
            $lang = app()->getLocale();
            $vacancies = Vacancy::where('lang', $lang)->get();

            $ids = [36,37,38];
            $carrer = [];
            foreach ($ids as $id){
                $page = $this->lang($id);
                $carrer[] = $page;
            }

            return view('templates.pages.pages')
                ->with([
                    'page' => $pages,
                    'vacancies' => $vacancies,
                    'career' => $carrer
                ]);
        }
        return view('templates.pages.pages')->with(['page' => $pages]);
    }

    public function ProductsPage()
    {
        $this->getLocale();
        $lang = app()->getLocale();

        $categories = Category::where('lang', $lang)->get();
        $products = Product::where('lang', $lang)->get();

        return view('templates.products.page')->with(['categories' => $categories, 'products' => $products]);
    }

    public function ProductInner($id)
    {
        $this->getLocale();
        $product = Product::where('id', $id)->first();
        return view('templates.products.inner')->with(['product' => $product]);
    }

    public function awards()
    {
        $this->getLocale();
        $awards = Award::get();
        return view('templates.awards.awards')->with(['awards' => $awards]);
    }

    public function logout()
    {
        $this->getLocale();
        auth()->logout();
        return redirect('/');
    }

    public function contact(Request $request)
    {
        $this->getLocale();

        $email = new \stdClass();
        $email->name = $request->name;
        $email->email = $request->email;
        $email->subject = $request->subject;
        $email->message = $request->message;
        $email->sender = 'GKD';
        $email->receiver = 'hr@gkd.uz';

        Mail::to("hr@gkd.uz")->send(new ContactEmail($email));

        return redirect()->back()->with('message', 'Сообщение успешно отправлено!');

    }
}
