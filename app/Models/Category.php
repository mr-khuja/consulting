<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function translations() {

        return $this->hasMany('App\Models\Category', 'category_id', 'id');

    }

    public function products() {

        if (is_numeric($this->category_id)){
            return $this->hasMany('App\Models\Product', 'category_id', 'category_id');
        } else {
            return $this->hasMany('App\Models\Product', 'category_id', 'id');
        }

    }
}
