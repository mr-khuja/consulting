<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function translations() {

        return $this->hasMany('App\Models\Feedback', 'feedback_id', 'id');

    }
}
