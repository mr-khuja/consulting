<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function children() {

        return $this->hasMany('App\Models\Menu', 'parent_id', 'id');

    }

    public function translations() {

        return $this->hasMany('App\Models\Menu', 'menu_id', 'id');

    }
}
