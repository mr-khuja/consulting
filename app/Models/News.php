<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function translations() {

        return $this->hasMany('App\Models\News', 'news_id', 'id');

    }
}
