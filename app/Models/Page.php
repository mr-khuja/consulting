<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function translations() {

        return $this->hasMany('App\Models\Page', 'page_id', 'id');

    }
}
