<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    protected $table = 'vacancies';
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function translations() {

        return $this->hasMany('App\Models\Vacancy', 'vacancy_id', 'id');

    }
}
