<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $lang = App()->getLocale();
        $configuration = DB::table('settings')->first();
        View::share('settings', $configuration);
        $contact = DB::table('pages')->find(2);
        View::share('contact', $contact);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
