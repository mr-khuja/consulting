<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id', 5)->autoIncrement();
            $table->string('title', 250);
            $table->string('path', 250);
            $table->string('lang', 10);
            $table->integer('order');
            $table->unsignedInteger('menu_id')->nullable();
            $table->foreign('menu_id')->references('id')->on('menu');
            $table->timestamps();
        });
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id', 5)->autoIncrement();
            $table->string('title', 250);
            $table->string('image')->nullable();
            $table->string('short')->nullable();
            $table->text('full')->nullable();
            $table->string('lang', 10);
            $table->unsignedInteger('news_id')->nullable();
            $table->foreign('news_id')->references('id')->on('news');
            $table->timestamps();
        });
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id', 5)->autoIncrement();
            $table->string('title', 250);
            $table->string('image')->nullable();
            $table->string('short')->nullable();
            $table->text('full')->nullable();
            $table->string('lang', 10);
            $table->integer('order');
            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products');
            $table->timestamps();
        });
        Schema::create('awards', function (Blueprint $table) {
            $table->increments('id', 5)->autoIncrement();
            $table->string('title', 250);
            $table->integer('order');
            $table->string('file');
            $table->timestamps();
        });
        Schema::create('partners', function (Blueprint $table) {
            $table->increments('id', 5)->autoIncrement();
            $table->string('title', 250);
            $table->string('image');
            $table->integer('order');
            $table->timestamps();
        });
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id', 5)->autoIncrement();
            $table->string('title', 250);
            $table->string('image')->nullable();
            $table->string('short')->nullable();
            $table->text('full')->nullable();
            $table->string('lang', 10);
            $table->unsignedInteger('page_id')->nullable();
            $table->foreign('page_id')->references('id')->on('pages');
            $table->timestamps();
        });
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->increments('id', 5)->autoIncrement();
            $table->string('title', 250);
            $table->string('image')->nullable();
            $table->text('full')->nullable();
            $table->string('lang', 10);
            $table->unsignedInteger('feedback_id')->nullable();
            $table->foreign('feedback_id')->references('id')->on('feedbacks');
            $table->timestamps();
        });
        Schema::create('slides', function (Blueprint $table) {
            $table->increments('id', 5)->autoIncrement();
            $table->string('title', 250);
            $table->string('image');
            $table->string('desc')->nullable();
            $table->string('link')->nullable();
            $table->string('lang', 10);
            $table->unsignedInteger('slide_id')->nullable();
            $table->foreign('slide_id')->references('id')->on('slides');
            $table->timestamps();
        });
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id', 5)->autoIncrement();
            $table->string('sitename', 250);
            $table->string('logo', 250);
            $table->string('favicon', 250)->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('address_uz', 250)->nullable();
            $table->string('address_en', 250)->nullable();
            $table->string('address_ru', 250)->nullable();
            $table->string('email', 250)->nullable();
            $table->longText('map')->nullable();
            $table->string('facebook', 250)->nullable();
            $table->string('instagram', 250)->nullable();
            $table->string('telegram', 250)->nullable();
            $table->string('skype', 250)->nullable();
            $table->string('twitter', 250)->nullable();
            $table->string('printerest', 250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
