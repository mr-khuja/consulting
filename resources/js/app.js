
require('./bootstrap');
import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);
import VueLocalForage from 'vue-localforage'
Vue.use(VueLocalForage);

import App from './App.vue';
import Home from './components/Home.vue';
import Settings from './components/Settings.vue';
import Social from './components/Socials.vue';
import Profile from './components/Profile.vue';
import Menu from './components/Menu/Menu.vue';
import Menu_c from './components/Menu/Menu_c.vue';
import Menu_e from './components/Menu/Menu_e.vue';
import Menu_t from './components/Menu/Menu_t.vue';
import Pages from './components/Pages/Pages.vue';
import Pages_c from './components/Pages/Pages_c.vue';
import Pages_e from './components/Pages/Pages_e.vue';
import Pages_t from './components/Pages/Pages_t.vue';
import Services from './components/Services/Services.vue';
import Services_c from './components/Services/Services_c.vue';
import Services_e from './components/Services/Services_e.vue';
import Services_t from './components/Services/Services_t.vue';
import Slides from './components/Slides/Slides.vue';
import Slides_c from './components/Slides/Slides_c.vue';
import Slides_e from './components/Slides/Slides_e.vue';
import Slides_t from './components/Slides/Slides_t.vue';
import Gallery from './components/Gallery/Gallery.vue';
import Gallery_c from './components/Gallery/Gallery_c.vue';
import Gallery_e from './components/Gallery/Gallery_e.vue';
import Partners from './components/Partners/Partners.vue';
import Partners_c from './components/Partners/Partners_c.vue';
import Partners_e from './components/Partners/Partners_e.vue';
import News from './components/News/News.vue';
import News_c from './components/News/News_c.vue';
import News_e from './components/News/News_e.vue';
import News_t from './components/News/News_t.vue';
import Products from './components/Products/Products.vue';
import Products_c from './components/Products/Products_c.vue';
import Products_e from './components/Products/Products_e.vue';
import Products_t from './components/Products/Products_t.vue';
import Vacancies from './components/Vacancies/Vacancies.vue';
import Vacancies_c from './components/Vacancies/Vacancies_c.vue';
import Vacancies_e from './components/Vacancies/Vacancies_e.vue';
import Vacancies_t from './components/Vacancies/Vacancies_t.vue';
import Categories from './components/Categories/Categories.vue';
import Categories_c from './components/Categories/Categories_c.vue';
import Categories_e from './components/Categories/Categories_e.vue';
import Categories_t from './components/Categories/Categories_t.vue';


const routes = [
    {
        path: '/admin',
        name: 'admin',
        component: Home
    },
    {
        path: '/admin/menu',
        name: 'Menu',
        component: Menu
    },
    {
        path: '/admin/menu/c',
        name: 'Menu_c',
        component: Menu_c
    },
    {
        path: '/admin/menu/t/:id/:lang',
        name: 'Menu_t',
        component: Menu_t
    },
    {
        path: '/admin/menu/e/:id',
        name: 'Menu_e',
        component: Menu_e
    },
    {
        path: '/admin/categories',
        name: 'Categories',
        component: Categories
    },
    {
        path: '/admin/categories/c',
        name: 'Categories_c',
        component: Categories_c
    },
    {
        path: '/admin/categories/t/:id/:lang',
        name: 'Categories_t',
        component: Categories_t
    },
    {
        path: '/admin/categories/e/:id',
        name: 'Categories_e',
        component: Categories_e
    },
    {
        path: '/admin/vacancies',
        name: 'Vacancies',
        component: Vacancies
    },
    {
        path: '/admin/vacancies/c',
        name: 'Vacancies_c',
        component: Vacancies_c
    },
    {
        path: '/admin/vacancies/t/:id/:lang',
        name: 'Vacancies_t',
        component: Vacancies_t
    },
    {
        path: '/admin/vacancies/e/:id',
        name: 'Vacancies_e',
        component: Vacancies_e
    },
    {
        path: '/admin/pages',
        name: 'Pages',
        component: Pages
    },
    {
        path: '/admin/pages/c',
        name: 'Pages_c',
        component: Pages_c
    },
    {
        path: '/admin/pages/e/:id',
        name: 'Pages_e',
        component: Pages_e
    },
    {
        path: '/admin/pages/t/:id/:lang',
        name: 'Pages_t',
        component: Pages_t
    },
    {
        path: '/admin/news',
        name: 'News',
        component: News
    },
    {
        path: '/admin/news/c',
        name: 'News_c',
        component: News_c
    },
    {
        path: '/admin/news/t/:id/:lang',
        name: 'News_t',
        component: News_t
    },
    {
        path: '/admin/news/e/:id',
        name: 'News_e',
        component: News_e
    },
    {
        path: '/admin/feedbacks',
        name: 'Services',
        component: Services
    },
    {
        path: '/admin/feedbacks/c',
        name: 'Services_c',
        component: Services_c
    },
    {
        path: '/admin/feedbacks/t/:id/:lang',
        name: 'Services_t',
        component: Services_t
    },
    {
        path: '/admin/feedbacks/e/:id',
        name: 'Services_e',
        component: Services_e
    },
    {
        path: '/admin/slides',
        name: 'Slides',
        component: Slides
    },
    {
        path: '/admin/slides/c',
        name: 'Slides_c',
        component: Slides_c
    },
    {
        path: '/admin/slides/t/:id/:lang',
        name: 'Slides_t',
        component: Slides_t
    },
    {
        path: '/admin/slides/e/:id',
        name: 'Slides_e',
        component: Slides_e
    },
    {
        path: '/admin/products',
        name: 'Products',
        component: Products
    },
    {
        path: '/admin/products/c',
        name: 'Products_c',
        component: Products_c
    },
    {
        path: '/admin/products/t/:id/:lang',
        name: 'Products_t',
        component: Products_t
    },
    {
        path: '/admin/products/e/:id',
        name: 'Products_e',
        component: Products_e
    },
    {
        path: '/admin/gallery',
        name: 'Gallery',
        component: Gallery
    },
    {
        path: '/admin/gallery/c',
        name: 'Gallery_c',
        component: Gallery_c
    },
    {
        path: '/admin/gallery/e/:id',
        name: 'Gallery_e',
        component: Gallery_e
    },
    {
        path: '/admin/partners',
        name: 'Partners',
        component: Partners
    },
    {
        path: '/admin/partners/c',
        name: 'Partners_c',
        component: Partners_c
    },
    {
        path: '/admin/partners/e/:id',
        name: 'Partners_e',
        component: Partners_e
    },
    {
        path: '/admin/social',
        name: 'Social',
        component: Social
    },
    {
        path: '/admin/settings',
        name: 'Settings',
        component: Settings
    },
    {
        path: '/admin/profile',
        name: 'Profile',
        component: Profile
    },
];
Vue.config.devtools = false
Vue.config.debug = false
Vue.config.silent = true
const router = new VueRouter({ mode: 'history', routes: routes});
new Vue(Vue.util.extend({ router }, App)).$mount('#app');
