<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<link rel="icon" href="images/favicon.ico" type="image/ico" />--}}

    <title>DataSite technology</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
<!-- Bootstrap -->
    <link href="/css/front/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/css/front/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/css/front/vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/css/front/build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('summernote/summernote-bs4.css') }}">
</head>
<body class="nav-md">
    <div class="container body">
        <div id="app">
        </div>
    </div>
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>
    <script src="/css/front/vendors/jquery/dist/jquery.min.js"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <!-- jQuery -->
    <!-- Bootstrap -->
    <script src="/css/front/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/css/front/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/css/front/vendors/nprogress/nprogress.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="/css/front/build/js/custom.js"></script>

</body>
</html>
