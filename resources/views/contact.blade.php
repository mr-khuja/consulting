@extends('layouts.app')

@section('content')
    <div style="height: 160px;"></div>
    <div class="page_title"
         style="background-image: url({{$page->baner}}) !important; background-repeat: no-repeat !important;">
        <div class="container">
            <div class="breadcrumbs">
                <span>
                    <a href="{{ route('home') }}" class="home" style="color: #fff;">@lang('Home')</a>
                </span>
                <span>
                    <i class="fa fa-angle-right" style="color: #fff;"></i>
                </span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name" style="color: #fff;">@lang('Contacts')</span>
                    <meta property="position" content="2">
                </span>
            </div>
            <h1 class="h2" style="color: #fff;">@lang('Contacts')</h1>
        </div>
    </div>
    <div class="container">
        <div class="content-area">
            <article id="post-1179" class="post-1179 page type-page status-publish hentry">
                <div class="entry-content">
                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1485430310469">
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-6">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="icon_box  style_3 clearfix">
                                        <div class="icon_box_inner">
                                            <div class="icon  font-color_default font-color_base_bg border-color_third">
                                                <i style="font-size:16px;  line-height: 48px; "
                                                   class="stm-phone_13_2"></i></div>
                                            <div class="icon_text">
                                                <p style="font-family: Montserrat, sans-serif;font-weight: 400;letter-spacing: -0.40px">
                                                    <span style="color: #595959">@lang('Call Us')</span><br>
                                                    <span style="font-size: 18px;color: #1a1a1a">+{{$settings->phone}}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-6">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="icon_box  style_3 clearfix">
                                        <div class="icon_box_inner">
                                            <div class="icon  font-color_default font-color_base_bg border-color_third">
                                                <i style="font-size:16px;  line-height: 48px; " class="fa fa-fax"></i>
                                            </div>
                                            <div class="icon_text">
                                                <p style="font-family: Montserrat, sans-serif;font-weight: 400;letter-spacing: -0.40px">
                                                    <span style="color: #595959">@lang('Fax')</span><br>
                                                    <span style="font-size: 18px;color: #1a1a1a">+{{$settings->fax}}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-6">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="icon_box  style_3 clearfix">
                                        <div class="icon_box_inner">
                                            <div class="icon  font-color_default font-color_base_bg border-color_third">
                                                <i style="font-size:16px;  line-height: 48px; "
                                                   class="fa fa-envelope"></i></div>
                                            <div class="icon_text">
                                                <p style="font-family: Montserrat, sans-serif;font-weight: 400;letter-spacing: -0.40px">
                                                    <span style="color: #595959">@lang('Have any questions?')</span><br>
                                                    <span style="color: #1a1a1a">{{$settings->email}}</span></p>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-6">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="icon_box  style_3 clearfix">
                                        <div class="icon_box_inner">
                                            <div class="icon  font-color_default font-color_base_bg border-color_third">
                                                <i style="font-size:16px;  line-height: 48px; " class="stm-marker"></i>
                                            </div>
                                            <div class="icon_text">
                                                <p style="font-family: Montserrat, sans-serif;font-weight: 400;letter-spacing: -0.40px">
                                                    <span style="color: #595959">
                                                        @php($array = json_decode(json_encode($settings), true))
                                                        {{$array['address_'.App()->getLocale()]}}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-vc-full-width="true" data-vc-full-width-init="true"
                         class="vc_row wpb_row vc_row-fluid vc_custom_1485495086300 vc_row-has-fill"
                         style="position: relative; left: -104.5px; box-sizing: border-box; width: 1349px; padding-left: 104.5px; padding-right: 104.5px;">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="vc_custom_heading vc_custom_1485497044589 text_align_left"><h2
                                            style="font-size: 20px;line-height: 30px;text-align: left;font-family:Montserrat;font-weight:400;font-style:normal">
                                            @lang('Leave a feedback')</h2></div>
                                    <div role="form" class="wpcf7" id="wpcf7-f1176-p1179-o1" lang="en-US" dir="ltr">
                                        <div class="screen-reader-response"></div>
                                        <form action="{{route('contact')}}" method="post"
                                              class="wpcf7-form">
                                            <div style="display: none;">
                                                @csrf
                                            </div>
                                            <div class="request_callback">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="input-group required ">
                                                            <input aria-required="true" required type="text"
                                                                   class="wpcf7-form-control" name="name"
                                                                   placeholder="@lang('Name')">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="input-group required ">
                                                            <input aria-required="true" required type="email"
                                                                   class="wpcf7-form-control" name="email"
                                                                   placeholder="@lang('E-mail')">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="input-group required ">
                                                            <input aria-required="true" required type="text"
                                                                   class="wpcf7-form-control" name="subject"
                                                                   placeholder="@lang('Subject')">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="input-group">
                                                            <textarea name="message" cols="30" rows="10"
                                                                class="wpcf7-form-control">@lang('Message')</textarea>
                                                            {{--<input type="email" class="wpcf7-form-control" name="name" placeholder="@lang('E-mail')">--}}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="input-group">
                                                            <button type="submit" class="button size-lg icon_right">
                                                                @lang('Submit') <i class="fa fa-chevron-right"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width vc_clearfix"></div>
                </div>

            </article>
        </div>

    </div>
@endsection
