@extends('layouts.app')

@section('content')
    @include('templates.home.slider')
    <div class="container">
        <div data-vc-full-width="true" data-vc-full-width-init="false"
             class="vc_row wpb_row vc_row-fluid third_bg_color vc_custom_1450692516354">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">

                        <div id="particles_5cc1be54be90d"></div>
                        <section class="vc_cta3-container">
                            <div
                                class="vc_general vc_cta3 third_bg_color vc_cta3-style-flat vc_cta3-shape-square vc_cta3-align-left vc_cta3-color-classic vc_cta3-icon-size-md vc_cta3-actions-right vc_custom_1495630569356">
                                <div class="vc_cta3_content-container">
                                    <div class="vc_cta3-content">
                                        <header class="vc_cta3-content-header">
                                            <div class="vc_custom_heading">
                                                <h2
                                                    style="font-size: 20px;color: #000000;line-height: 24px">
                                                    @lang('Ask us a question')</h2>
                                            </div>
                                        </header>
                                    </div>
                                    <div class="vc_cta3-actions">
                                        <div class="vc_btn3-container vc_btn3-right">
                                            <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-block vc_btn3-icon-right vc_btn3-color-theme_style_2"
                                               href="{{route('page', $contact->id)}}"
                                               title="">@lang('Contact us') <i
                                                    class="vc_btn3-icon fa fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        @include('templates.home.about')
        @include('templates.home.products')
        <div class="vc_row-full-width vc_clearfix"></div>
        @include('templates.home.counters')
        <div class="vc_row-full-width vc_clearfix"></div>
{{--        @include('templates.home.feedbacks')--}}
        @include('templates.home.partners')
        <div class="vc_row-full-width vc_clearfix"></div>
        @include('templates.home.news')
        <div class="vc_row-full-width vc_clearfix"></div>
        @include('templates.home.contact_form')

        <div class="vc_row-full-width vc_clearfix"></div>
        <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true"
             class="vc_row wpb_row vc_row-fluid vc_custom_1453109987955 vc_row-no-padding"
             style="position: relative; left: -104.5px; box-sizing: border-box; width: 1349px;">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div id="map_5ce284f08122a" class=" stm_gmap_wrapper">
                            <div style="height: 447px; position: relative; overflow: hidden;"
                                 id="stm-gmap-5ce284f0811eb" class="stm_gmap">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3007.0614481969847!2d69.04225571482867!3d41.0895040223724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDHCsDA1JzIyLjIiTiA2OcKwMDInNDAuMCJF!5e0!3m2!1sru!2s!4v1558353425123!5m2!1sru!2s"
                                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
{{--        <div class="vc_row wpb_row vc_row-fluid vc_custom_1483436740696" data-vc-full-width="true"--}}
{{--             data-vc-full-width-init="false">--}}
{{--        </div>--}}
        <div class="vc_custom_1453109987955"></div>
        {{--<div data-vc-full-width="true" data-vc-full-width-init="false"--}}
        {{--class="vc_row wpb_row vc_row-fluid overlay_2 fixed_bg vc_custom_1453109987955  vc_row-no-padding">--}}
        {{--<div class="wpb_column vc_column_container vc_col-sm-12">--}}
        {{--<div class="vc_column-inner ">--}}
        {{--<div class="wpb_wrapper">--}}
        {{--<div class="vc_custom_heading text_align_center">--}}
        {{--<h4--}}
        {{--style="font-size: 28px;color: #ffffff;line-height: 34px;text-align: center">--}}
        {{--To help entrepreneurs get their act together<br/>--}}
        {{--<mark>before they talk to investors.</mark>--}}
        {{--</h4>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="vc_row-full-width vc_clearfix"></div>
    </div>
@endsection
