<!DOCTYPE html>
<html lang="en-US" class="stm-site-preloader">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$settings->sitename}}</title>
    {{--<script type='text/javascript' src='js/google.js'></script>--}}

    {{--<script type='text/javascript'--}}
    {{--src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAfGIHJLOELRXNVf6-EbsaemhRfZUdPtrY&#038;ver=6.7'></script>--}}
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,600,700,800&amp;subset=cyrillic"
          rel="stylesheet">
    <script src="/js/scripts.min.js"></script>
    <script type="text/javascript" src="/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="/js/jquery.themepunch.revolution.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/settings.css" media="screen" />
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body
    class="home page-template-default page page-id-2 woocommerce-no-js site_layout_1  header_style_2 sticky_menu header_transparent wpb-js-composer js-comp-ver-5.6 vc_responsive">
<div id="wrapper">
    <div id="fullpage" class="content_wrapper">
        @include('layouts.header')
        <div id="main">
        @yield('content')
        <!--.container-->
        </div>
        <!--#main-->
    </div>
    <!--.content_wrapper-->
    @include('layouts.footer')


</div>
<!--#wrapper-->

<link crossorigin="anonymous" rel='stylesheet' id='vc_google_fonts_abril_fatfaceregular-css'
      href='//fonts.googleapis.com/css?family=Abril+Fatface%3Aregular&#038;ver=5.0.4' type='text/css' media='all'/>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var screenWidth = $(window).width();
        if (screenWidth < 1140) {
            var defaultWidth = screenWidth;
        } else {
            var defaultWidth = 1140;
        }
        var marginLeft = (screenWidth -
            defaultWidth) / 2;
        $('#particles_5cc1be54be90d').css({
            'width': screenWidth + 'px',
            'margin-left': '-' +
                marginLeft + 'px'
        });
        particlesJS('particles_5cc1be54be90d', {
            "particles": {
                "number": {
                    "value": 120,
                    "density": {
                        "enable": true,
                        "value_area": 800
                    }
                },
                "color": {
                    "value": "#ffffff"
                },
                "opacity": {
                    "value": 0.5,
                    "random": false,
                    "anim": {
                        "enable": false,
                        "speed": 1,
                        "opacity_min": 0.1,
                        "sync": false
                    }
                },
                "size": {
                    "value": 8,
                    "random": true,
                    "anim": {
                        "enable": false,
                        "speed": 40,
                        "size_min": 0.1,
                        "sync": false
                    }
                },
                "line_linked": {
                    "enable": true,
                    "distance": 150,
                    "color": "#ffffff",
                    "opacity": 0.4,
                    "width": 1
                },
                "move": {
                    "enable": true,
                    "speed": 6,
                    "direction": "none",
                    "random": false,
                    "straight": false,
                    "out_mode": "out",
                    "attract": {
                        "enable": false,
                        "rotateX": 600,
                        "rotateY": 1200
                    }
                }
            },
            "interactivity": {
                "detect_on": "canvas",
                "events": {
                    "onhover": {
                        "enable": true,
                        "mode": "grab"
                    },
                    "onclick": {
                        "enable": true,
                        "mode": "push"
                    },
                    "resize": true
                }
            },
            "retina_detect": true
        });
    });

    jQuery(document).ready(function ($) {
        "use strict";
        var
            partners_carousel_5cc1be54ce0b5 = $("#partners_carousel_5cc1be54ce0b5");
        var slickRtl = false;
        if ($('body').hasClass('rtl')) {
            slickRtl = true;
        }
        partners_carousel_5cc1be54ce0b5.slick({
            rtl: slickRtl,
            dots: false,
            infinite: true,
            arrows: true,
            prevArrow: "<div class=\" slick_prev\"><i class=\"fa fa-chevron-left\"></i></div>",
            nextArrow: "<div class=\"slick_next\"><i class=\"fa fa-chevron-right\"></i></div>",
            autoplaySpeed: 5000,
            autoplay: false,
            slidesToShow: 2,
            cssEase: "cubic-bezier(0.455, 0.030, 0.515, 0.955)",
            responsive: [{
                breakpoint: 769,
                settings: {
                    slidesToShow: 1
                }
            }]
        });
    });

    jQuery(document).ready(function ($) {
        $(window).load(function () {
            var owlRtl = false;
            if ($('body').hasClass('rtl')) {
                owlRtl = true;
            }
            $("#owl-5cc1be552a9b3").owlCarousel({
                rtl: owlRtl,
                dots: false,
                autoplayTimeout: 5000,
                smartSpeed: 250,
                responsive: {
                    0: {
                        items: 1
                    },
                    768: {
                        items: 4
                    },
                    980: {
                        items: 5
                    },
                    1199: {
                        items: 6
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    var revapi;

    jQuery(document).ready(function () {

        revapi = jQuery('.tp-banner').revolution({
            startwidth: 1170,
            startheight: 667,
            hideThumbs: 10,
            fullWidth: "on",
            fullScreen: "off",
            sliderLayout: "fullscreen",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                mouseScrollReverse: "default",
                onHoverStop: "off",
                touch: {
                    touchenabled: "on",
                    touchOnDesktop: "on",
                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    swipe_direction: "horizontal",
                    drag_block_vertical: false
                },
                arrows: {
                    style: "",
                    enable: true,
                    tmp: '',
                    left: {
                        h_align: "left",
                        v_align: "center",
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                    }
                }
            },
            lazyType: "smart",
            shadow: 0,
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            fullScreenAutoWidth: "off",
            fullScreenAlignForce: "off",
            fullScreenOffsetContainer: "",
            fullScreenOffset: "",
            disableProgressBar: "on",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: "true",
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
            }
        });

    }); //ready
</script>
@isset($vacancies)
    @foreach($vacancies as $key => $vac)
        <div class="modal fade bd-example-modal-lg" id="career-modal-{{$vac->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h5 class="modal-title" id="exampleModalLabel">{{$vac->title}}</h5>
                        {!! $vac->body !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-rounded vc_btn3-style-outline vc_btn3-color-theme_style_2" data-dismiss="modal">@lang('Close')</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
</body>

</html>
