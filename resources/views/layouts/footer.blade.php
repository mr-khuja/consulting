<footer id="footer" class="footer style_1">

    <div class="widgets_row">
        <div class="container">
            <div class="footer_widgets">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="footer_logo">
                            <a href="/">
                                <img src="{{$settings->logo}}"/>
                            </a>
                        </div>
                        <div class="footer_text">
                            <p>{{$abt->short}}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <section id="recent-posts-4" class="widget widget_recent_entries">
                            <h4 class="widget_title no_stripe">@lang('recent news')</h4>
                            <ul>
                                @foreach($news as $key => $new)
                                    @if($key == 0 || $key == 1)
                                        <li>
                                            <a
                                                href="{{route('news', $new->id)}}">{{$new->title}}</a>
                                            <span
                                                class="post-date">{{date('d.m.Y', strtotime($new->created_at))}}</span>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </section>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <section id="nav_menu-2" class="widget widget_nav_menu">
                            <h4 class="widget_title no_stripe">@lang('extra links')</h4>
                            <div class="menu-extra-links-container">
                                <ul id="menu-extra-links" class="menu">
                                    @foreach($menu as $item)
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-163">
                                            <a
                                                href="{{$item->path}}">{{$item->title}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright_row">
        <div class="container">
            <div class="copyright_row_wr">
                <div class="socials">
                    <ul>
                        @php($socar = json_decode(json_encode($settings), true))
                        @php($socials = ['facebook' => 'fab fa-facebook-square', 'instagram' => 'fab fa-instagram', 'telegram' => 'fab fa-telegram', 'skype' => 'fab fa-skype', 'twitter' => 'fab fa-facebook-square', 'printerest' => 'fab fa-printerest-square'])
                        @foreach($socials as $social => $class)
                            @if(strlen($socar[$social]) > 7)
                                <li>
                                    <a href="{{$socar[$social]}}" target="_blank" class="social-{{$social}}">
                                        <i class="{{$class}} fa"></i>
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="copyright">
                    © {{date('Y')}} <a href="/" target="_blank">{{$settings->sitename}}</a>
                </div>
            </div>
        </div>
</footer>
