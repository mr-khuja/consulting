<header id="header">
    <div class="top_bar">
        <div class="container">
            <div id="lang_sel">
                <ul>
                    @php($langs = ['en' => 'English', 'ru' => 'Русский'])
                    <li><a href="#" class="lang_sel_sel icl-en">{{$langs[app()->getLocale()]}}</a>
                        <ul>
                            <li class="icl-fr"><a href="/setlocale/en">English</a></li>
                            <li class="icl-de"><a href="/setlocale/ru">Русский</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="top_bar_info_wr">
                <ul class="top_bar_info" id="top_bar_info_1" style="display: block;">
                    <li>
                        <i class="stm-marker"></i>
                        <span>
                                    @php($array = json_decode(json_encode($settings), true))
                            {{$array['address_'.App()->getLocale()]}}
                                </span>
                    </li>
                    <li>
                        <i class="fa fa-envelope"></i>
                        <span>
										{{$settings->email}} </span>
                    </li>
                    <li>
                        <i class="fa fa-phone"></i>
                        <span>
										+{{$settings->phone}}</span>
                    </li>
                </ul>
                <ul class="top_bar_info" id="top_bar_info_2">
                    <li>
                        <i class="stm-marker"></i>
                        <span>
                                    @php($array = json_decode(json_encode($settings), true))
                            {{$array['address_'.App()->getLocale()]}}
                                </span>
                    </li>
                    <li>
                        <i class="fa fa-envelope"></i>
                        <span>
										{{$settings->email}} </span>
                    </li>
                    <li>
                        <i class="fa fa-phone"></i>
                        <span>
										+{{$settings->phone}}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header_top clearfix">
        <div class="container">
            <div class="logo media-left media-middle">
                <a href="/"><img
                        src="{{$settings->logo}}"
                        style="width: 170px; height: auto;" alt="Consulting WP &#8211; New York"/></a>
            </div>
            <div class="top_nav media-body media-middle">
                <div class="header_socials">
                    @php($socar = json_decode(json_encode($settings), true))
                    @php($socials = ['facebook' => 'fab fa-facebook-square', 'instagram' => 'fab fa-instagram', 'telegram' => 'fab fa-telegram', 'skype' => 'fab fa-skype', 'twitter' => 'fab fa-facebook-square', 'printerest' => 'fab fa-printerest-square'])
                    @foreach($socials as $social => $class)
                        @if(strlen($socar[$social]) > 7)
                            <a href="{{$socar[$social]}}" target="_blank">
                                <i class="{{$class}} fa"></i>
                            </a>
                        @endif
                    @endforeach
                </div>
                <div class="top_nav_wrapper clearfix">
                    <ul id="menu-main-menu" class="main_menu_nav">
                        @foreach($menu as $item)
                            @if(count($item->children) > 0 && ($item->id != 6 || $item->menu_id != 6) && ($item->id != 35 || $item->menu_id != 35))
                                <li id="menu-item-281"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-281">
                                    <a>{{$item->title}}</a>
                                    <ul class="sub-menu">
                                        @foreach($item->children as $child)
                                            <li id="menu-item-379"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-603 stm_col_width_default stm_mega_cols_inside_default">
                                                <a href="{{$child->path}}">
                                                    {{$child->title}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @elseif($item->id == 35 || $item->menu_id == 35)
                                <li id="menu-item-179"
                                    class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-179 stm_megamenu stm_megamenu__boxed stm_megamenu_default">
                                    <a>{{$item->title}}</a>

                                    <ul class="sub-menu products-sub-menu">
                                        @foreach($cats as $itm)
                                            <li>
                                                <a href="/products-page#tab-{{$itm->id}}">
                                                    <div class="col-xs-6">
                                                        <img src="{{$itm->image}}">
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <h5 class="product-title">
                                                            {{$itm->title}}
                                                        </h5>
                                                        <p class="description">
                                                            {{$itm->short}}
                                                        </p>
                                                    </div>
                                                </a>
                                            </li>
{{--                                            <li id="menu-item-1670"--}}
{{--                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1670 stm_col_width_default stm_mega_cols_inside_default">--}}
{{--                                                <a href="/products-page#tab-{{$item->id}}">{{$itm->title}}</a>--}}
{{--                                                <ul class="sub-menu">--}}
{{--                                                    <li id="menu-item-1671"--}}
{{--                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1671 stm_mega_second_col_width_default">--}}
{{--                                                        <a href="/products-page#tab-{{$item->id}}"><img--}}
{{--                                                                alt="Company Overview1"--}}
{{--                                                                src="{{$itm->image}}">--}}
{{--                                                            <div class="stm_mega_textarea">{{$itm->short}}</div>--}}
{{--                                                        </a>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </li>--}}
                                        @endforeach
                                    </ul>
                                </li>
                            @elseif($item->id == 6 || $item->menu_id == 6)
                                <li id="menu-item-179"
                                    class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-179 stm_megamenu stm_megamenu__boxed stm_megamenu_default">
                                    <a>{{$item->title}}</a>

                                    <ul class="sub-menu">
                                        @foreach($submenu as $itm)
                                            <li id="menu-item-1670"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1670 stm_col_width_default stm_mega_cols_inside_default">
                                                <a href="{{ route('page', $itm->id) }}">{{$itm->title}}</a>
                                                <ul class="sub-menu">
                                                    <li id="menu-item-1671"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1671 stm_mega_second_col_width_default">
                                                        <a href="{{ route('page', $itm->id) }}"><img
                                                                alt="Company Overview1"
                                                                src="{{$itm->image}}">
                                                            <div class="stm_mega_textarea">{{$itm->short}}</div>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li id="menu-item-13"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2 current_page_item menu-item-13">
                                    <a href="{{$item->path}}">{{$item->title}}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="mobile_header">
        <div class="logo_wrapper clearfix">
            <div class="logo">
                <a href="/"><img
                        src="{{$settings->logo}}"
                        style="width: 170px; height: auto;"/></a>
            </div>
            <div id="menu_toggle">
                <button></button>
            </div>
        </div>
        <div class="header_info">
            <div class="top_nav_mobile">
                <ul id="menu-main-menu-1" class="main_menu_nav">
                    @foreach($menu as $item)
                        @if(count($item->children) > 0)
                            <li
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-467">
                                <a href="#">{{$item->title}}</a>
                                <ul class="sub-menu">
                                    @foreach($item->children as $child)
                                        <li
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1402 stm_col_width_default stm_mega_cols_inside_default">
                                            <a href="{{$child->path}}">{{$child->title}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @else
                            <li
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2 current_page_item menu-item-13">
                                <a href="{{$item->path}}">{{$item->title}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <div class="icon_texts">
                <div class="icon_text clearfix">
                    <div class="icon"><i class="fa stm-phone"></i>
                    </div>
                    <div class="text">
                        <strong>+{{$settings->phone}}</strong></div>
                </div>
                <div class="icon_text clearfix">
                    <div class="icon"><i class="fa fa fa-envelope"></i>
                    </div>
                    <div class="text">
                        <strong>{{$settings->email}}</strong></div>
                </div>
                <div class="icon_text clearfix">
                    <div class="icon"><i class="fa stm-marker"></i>
                    </div>
                    <div class="text">
                        <strong>{{$array['address_'.App()->getLocale()]}}</strong>
                    </div>
                </div>
            </div>

        </div>
    </div>
</header>
