@extends('layouts.app')

@section('content')
    <div style="height: 160px;"></div>
    <div class="page_title"
         style="background-image: url(/images/awards.jpg) !important; background-repeat: no-repeat !important;">
        <div class="container">
            <div class="breadcrumbs">
                <span>
                    <a href="{{ route('home') }}" class="home">@lang('Home')</a>
                </span>
                <span><i class="fa fa-angle-right"></i></span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name">@lang('Awards and certificates')</span>
                    <meta property="position" content="2">
                </span>
            </div>
            <h1 class="h2">@lang('Awards and certificates')</h1>
        </div>
    </div>
    <div class="container">

        <div class="content-area">

            <article id="post-1130" class="post-1130 page type-page status-publish hentry">

                <div class="entry-content">
                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1485854969371">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="stm_portfolio_grid">
                                        @foreach($awards as $item)
                                            <div class="item default">
                                                <div class="item_thumbnail has-thumbnail">
                                                    <img src="{{$item->file}}" width="100%" alt="portfolio-grid1"
                                                         title="portfolio-grid1">
                                                    <a class="colorbox" href="{{$item->file}}">
                                                    <span class="portfolio-title">
                                                        {{$item->title}}
                                                    </span>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width vc_clearfix"></div>
                </div>

            </article>
        </div>

    </div>
    <script src="/js/jquery.colorbox-min.js"></script>
    <link rel="stylesheet" href="/css/colorbox.css">
    <script>
        jQuery(document).ready(function(){
            jQuery('.colorbox').colorbox();
        });
    </script>
@endsection
