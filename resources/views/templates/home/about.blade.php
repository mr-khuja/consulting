<div class="about-us">
    <div class="vc_tta-panel-body">
        <div class="vc_row wpb_row vc_inner vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="wpb_single_image wpb_content_element vc_align_center">

                            <figure class="wpb_wrapper vc_figure">
                                <a data-rel="prettyPhoto[rel-1071-466209147]"
                                   class="vc_single_image-wrapper   vc_box_border_grey prettyphoto">
                                    <img
                                        class="vc_single_image-img "
                                        src="{{ $about['page']->image}}"
                                        width="530" height="432" alt="sev1" title="sev1">
                                </a>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div
                            class="vc_custom_heading vc_custom_1540883346465 text_align_left title_no_stripe">
                            <h3 style="text-align: left">{{$about['page']->title}}</h3>
                        </div>
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <p style="font-size: 14px; line-height: 24px; color: #707070;">
                                    {{$about['page']->short}}
                                </p>
                            </div>
                        </div>
                        <div class="vc_btn3-container vc_btn3-inline">
                            <a class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-rounded vc_btn3-style-outline vc_btn3-color-theme_style_2"
                               href="{{route('page', $about['page']->id)}}"
                               title="">@lang('Read more')</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="vc_row wpb_row vc_row-fluid vc_custom_1450856873482">
    <div class="wpb_column vc_column_container vc_col-sm-4">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="stm_animation stm_viewport" style="" data-animate="fadeInUp"
                     data-animation-delay="0" data-animation-duration="1"
                     data-viewport_position="90">

                    <div class="info_box style_3 ">


                        <div class="info_box_wrapper">


                            <div class="info_box_image"><img width="350" height="250"
                                                             src="/images/ms.png"
                                                             class="attachment-consulting-image-350x250-croped size-consulting-image-350x250-croped"
                                                             alt=""/></div>

                            <div class="info_box_text">

                                <div class="title">
                                    <div class="icon">
                                        <i class="stm-chart-monitor"></i>
                                    </div>
                                    <h6 class="no_stripe"><span>{{$about['mission']->title}}</span></h6>
                                </div>
                                <p>{{$about['mission']->short}}</p>
                                <a class="read_more" target="_self"
                                   href="{{route('page', $about['mission']->id)}}"><span>@lang('Read more')</span><i
                                        class=" fa fa-chevron-right stm_icon"></i></a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-4">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="stm_animation stm_viewport" style="" data-animate="fadeInUp"
                     data-animation-delay="0.05" data-animation-duration="1"
                     data-viewport_position="90">

                    <div class="info_box style_3 ">


                        <div class="info_box_wrapper">


                            <div class="info_box_image"><img width="350" height="250"
                                                             src="/images/hs.png"
                                                             class="attachment-consulting-image-350x250-croped size-consulting-image-350x250-croped"
                                                             alt=""/></div>

                            <div class="info_box_text">

                                <div class="title">
                                    <div class="icon">
                                        <i class="stm-chart-refresh"></i>
                                    </div>
                                    <h6 class="no_stripe"><span>{{$about['history']->title}}</span></h6>
                                </div>
                                <p>{{$about['history']->short}}</p>
                                <a class="read_more" target="_self"
                                   href="{{route('page', $about['history']->id)}}"><span>@lang('Read more')</span><i
                                        class=" fa fa-chevron-right stm_icon"></i></a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-4">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="stm_animation stm_viewport" style="" data-animate="fadeInUp"
                     data-animation-delay="0.1" data-animation-duration="1"
                     data-viewport_position="90">

                    <div class="info_box style_3 ">


                        <div class="info_box_wrapper">


                            <div class="info_box_image"><img width="350" height="250"
                                                             src="/images/aw.png"
                                                             class="attachment-consulting-image-350x250-croped size-consulting-image-350x250-croped"
                                                             alt=""/></div>

                            <div class="info_box_text">

                                <div class="title">
                                    <div class="icon">
                                        <i class="stm-earth"></i>
                                    </div>
                                    <h6 class="no_stripe"><span>@lang('Awards and certificates')</span></h6>
                                </div>
                                <p>@lang('Preview our awards and certificates')</p>
                                <a class="read_more" target="_self"
                                   href="{{route('awards')}}"><span>@lang('Read more')</span><i
                                        class=" fa fa-chevron-right stm_icon"></i></a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
