<div data-vc-full-width="true" data-vc-full-width-init="false"
     class="vc_row wpb_row vc_row-fluid vc_custom_1451300723020 vc_row-has-fill">
    <div class="wpb_column vc_column_container vc_col-sm-5">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="vc_custom_heading vc_custom_1495776904400 text_align_left">
                    <h3
                        style="font-size: 36px;color: #ffffff;line-height: 38px;text-align: left">
                        @lang('Request a call back.')</h3>
                </div>
                <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper  stm_gmap_wrapper">
                        <div class="gmap_addresses" style="padding: 0; background: #222; color: #fff;">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                        <div class="addresses_wr">
                                            <div class="addresses" id="owl_5cc1be553a6be">

                                                <div class="item" data-lat="51.507351"
                                                     data-lng="-0.127758"
                                                     data-title="United Kingdom">
                                                    <div class="title">@lang('Contacts')</div>
                                                    <ul class="asda">
                                                        <style>
                                                            .asda li{
                                                                padding-left: 0 !important;
                                                            }
                                                            .asda li:before{
                                                                display: none !important;
                                                            }
                                                        </style>
                                                        <li>
                                                            <div class="icon">
                                                                <i
                                                                    class="stm-location-2"></i>
                                                            </div>
                                                            <div class="text">
                                                                <p style="color: #fff;">
                                                                    @php($array = json_decode(json_encode($settings), true))
                                                                    {{$array['address_'.App()->getLocale()]}}
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="icon">
                                                                <i class="stm-phone"></i>
                                                            </div>
                                                            <div class="text">
                                                                <p style="color: #fff;">+{{$settings->fax}}</p>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="icon">
                                                                <i class="stm-iphone"></i>
                                                            </div>
                                                            <div class="text">
                                                                <p style="color: #fff;">+{{$settings->phone}}</p>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="icon">
                                                                <i class="stm-email"></i>
                                                            </div>
                                                            <div class="text">
                                                                <a
                                                                    href="mailto:{{$settings->email}}">{{$settings->email}}</a>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-7">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div role="form" class="wpcf7" id="wpcf7-f4-p2-o1" lang="en-US"
                     dir="ltr">
                    <div class="screen-reader-response"></div>
                    <form action="{{route('contact')}}" method="post" class="wpcf7-form">
                        <div style="display: none;">
                            @csrf
                        </div>
                        <div class="request_callback">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group required ">
                                        <input aria-required="true" required type="text" class="wpcf7-form-control" name="name"
                                               placeholder="@lang('Name')">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group required ">
                                        <input aria-required="true" required type="email" class="wpcf7-form-control" name="email"
                                               placeholder="@lang('E-mail')">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <div class="input-group required ">
                                        <input aria-required="true" required type="text" class="wpcf7-form-control" name="subject"
                                               placeholder="@lang('Subject')">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <textarea name="message" cols="30" rows="10"
                                                  class="wpcf7-form-control">@lang('Message')</textarea>
                                        {{--<input type="email" class="wpcf7-form-control" name="name" placeholder="@lang('E-mail')">--}}
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <button type="submit"
                                                class="button size-lg icon_right">@lang('Submit') <i
                                                class="fa fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                    </form>
                    <script src="//helpa.dst.uz/js/validate.js"></script>
                </div>
            </div>
        </div>
    </div>

</div>
