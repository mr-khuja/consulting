<div data-vc-full-width="true" data-vc-full-width-init="false"
     class="vc_row wpb_row vc_row-fluid overlay fixed_bg vc_custom_1483436740696 vc_row-has-fill">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div
                                    class="vc_custom_heading no_stripe vc_custom_1495714664917 text_align_left has_icon">
                                    <div class="icon"
                                         style="font-size: 67px; line-height: 67px;"><i
                                            class="stm-check"></i></div>
                                    <h4
                                        style="font-size: 45px;color: #ffffff;line-height: 45px;text-align: left">
{{--                                        <mark>@lang('The company\'s total annual paper production is more than') {{$settings->fifth}}--}}
{{--                                            @lang('tons. Up to now') {{$settings->sixth}} @lang('tons produced.')</mark>--}}
{{--                                        <br/>--}}
{{--                                        @lang('This comprises 45% of Asian paper'):--}}
                                        @lang('Monthly cotton pulp production statistics')
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                    <div
                        class="wpb_column vc_column_container vc_col-sm-3 vc_col-lg-offset-1">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="stats_counter style_1 left ">
                                    <div class="inner">
                                        <h3 class="no_stripe"
                                            id="counter_5cc1be54cab43">0</h3>
                                        <div class="counter_title">@lang('Our goal')</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="stats_counter style_1 left ">
                                    <div class="inner">
                                        <h3 class="no_stripe"
                                            id="counter_5cc1be54cb140">0</h3>
                                        <div class="counter_title">@lang('Achieved')</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="stats_counter style_1 left ">
                                    <div class="inner">
                                        <h3 class="no_stripe"
                                            id="counter_5cc1be54cb6c6">0</h3>
                                        <div class="counter_title">@lang('This month result')</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-2">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="stats_counter style_1 left ">
                                    <div class="inner">
                                        <h3 class="no_stripe"
                                            id="counter_5cc1be54cbc24">0</h3>
                                        <div class="counter_title">@lang('Overall result')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function ($) {
            var
                counter_5cc1be54cab43 = new countUp("counter_5cc1be54cab43", 0, {{$settings->first}}, 0, 2.5, {
                    useEasing: true,
                    useGrouping: false,
                    prefix: '',
                    suffix: 't'
                });
            $(window).scroll(function () {
                if ($("#counter_5cc1be54cab43").is_on_screen()) {
                    counter_5cc1be54cab43.start();
                }
            });
        });
        jQuery(document).ready(function ($) {
            var counter_5cc1be54cb140 = new
            countUp("counter_5cc1be54cb140", 0, {{$settings->second}}, 0, 2.5, {
                useEasing: true,
                useGrouping: false,
                prefix: '',
                suffix: '%'
            });
            $(window).scroll(function () {
                if ($("#counter_5cc1be54cb140").is_on_screen()) {
                    counter_5cc1be54cb140.start();
                }
            });
        });
        jQuery(document).ready(function ($) {
            var counter_5cc1be54cb6c6 = new countUp("counter_5cc1be54cb6c6", 0,
                {{$settings->third}}, 0, 2.5, {
                    useEasing: true,
                    useGrouping: false,
                    prefix: '',
                    suffix: 't'
                });
            $(window).scroll(function () {
                if ($("#counter_5cc1be54cb6c6").is_on_screen()) {
                    counter_5cc1be54cb6c6.start();
                }
            });
        });
        jQuery(document).ready(function ($) {
            var counter_5cc1be54cbc24 = new countUp("counter_5cc1be54cbc24", 0, {{$settings->fourth}}, 0,
                2.5, {
                    useEasing: true,
                    useGrouping: false,
                    prefix: '',
                    suffix: 't'
                });
            $(window).scroll(function () {
                if ($("#counter_5cc1be54cbc24").is_on_screen()) {
                    counter_5cc1be54cbc24.start();
                }
            });
        });
    </script>
</div>
