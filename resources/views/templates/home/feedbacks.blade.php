<div class="vc_row wpb_row vc_row-fluid vc_custom_1450960276268">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div
                    class="vc_custom_heading base_font_color vc_custom_1495714677591 text_align_center">
                    <h2 style="font-size: 45px;line-height: 60px;text-align: center">
                        @lang('Feedbacks')</h2>
                </div>
                <div class="testimonials_carousel style_1 per_row_2"
                     id="partners_carousel_5cc1be54ce0b5">
                    @foreach($feedbacks as $item)
                        <div class="testimonial">
                        <div class="image">
                            <a>
                                <img
                                    class=""
                                    src="{{$item->image}}"
                                    width="350" height="348" alt="testimonial-2"
                                    title="testimonial-2"/></a>
                        </div>
                        <div class="info">
                            <h4 class="no_stripe">
                                <a>{{$item->title}}</a>
                            </h4>
                            {!! $item->full !!}
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
