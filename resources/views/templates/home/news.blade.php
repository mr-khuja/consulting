<div class="vc_row wpb_row vc_row-fluid vc_custom_1451479401686">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="vc_custom_heading text_align_center">
                    <h2 style="font-size: 45px;text-align: center">@lang('News')</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="vc_row wpb_row vc_row-fluid vc_custom_1451043728133">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="stm_news ">
                    <ul class="news_list posts_per_row_4">
                        @foreach($news as $item)
                            <li>
                                <div class="post_inner">
                                    <div class="image">
                                        <a
                                            href="{{route('news', $item->id)}}">
                                            <img width="350" height="250"
                                                 src="{{$item->image}}"
                                                 class="attachment-consulting-image-350x250-croped size-consulting-image-350x250-croped wp-post-image"/>
                                        </a>
                                    </div>
                                    <div class="stm_news_unit-block">
                                        <h5 class="no_stripe">
                                            <a href="{{route('news', $item->id)}}">
                                                {{$item->title}}
                                            </a>
                                        </h5>
                                        <div class="date">
                                            <span class="the_date">{{date('d.m.Y', strtotime($item->created_at))}}</span>
                                            <a href="{{route('news', $item->id)}}"
                                               class="button size-lg hidden">@lang('Read more')</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
