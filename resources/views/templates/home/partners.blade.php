<div data-vc-full-width="true" data-vc-full-width-init="false"
     class="vc_row wpb_row vc_row-fluid vc_custom_1450961224119 vc_row-has-fill">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="vc_image_carousel_wr grayscale no_paddings">
                    <div class="vc_image_carousel style_1" id="owl-5cc1be552a9b3">
                        @foreach($partners as $partner)
                            <div class="item">
                                <a>
                                    <img src="{{$partner->image}}"
                                         style="width: 100%; height: auto;"/>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
