<div class="vc_row wpb_row vc_row-fluid">
    <div class="vc_column-inner ">
        <div class="wpb_wrapper">
            <div
                class="vc_custom_heading base_font_color vc_custom_1495714677591 text_align_center">
                <h2 style="font-size: 45px;line-height: 60px;text-align: center">
                    @lang('Products')</h2>
            </div>
            <div class="stm_services style_1 cols_3">
                @foreach($products as $product)
                    <div class="item">
                        <div class="item_wr">

                            <div class="item_thumbnail">
                                <a href="{{ route('product', $product->id) }}">
                                    <img width="100%"
                                         src="{{ $product->image }}"
                                         class="attachment-consulting-image-255x182-croped" alt=""
                                         srcset="{{ $product->image }}">
                                </a>
                            </div>
                            <div class="content">
                                <h5><a href="{{ route('product', $product->id) }}">{{ $product->title }}</a>
                                </h5>
                                <p>{{ $product->short }}</p>
                                <a class="read_more"
                                   href="{{ route('product', $product->id) }}">
                                    <span>@lang('Read more')</span>
                                    <i class=" fa fa-chevron-right stm_icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
