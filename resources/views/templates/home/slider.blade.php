<div class="rev_slider_wrapper fullscreen-container">
    <div data-vc-full-width="true" data-vc-full-width-init="false" class="tp-banner">
        <ul>
            @foreach($baner as $item)
            <!-- SLIDE  -->
                <li data-transition="random" data-slotamount="5" data-masterspeed="700">
                    <!-- MAIN IMAGE -->
                    <img src="{{$item->image}}" alt="slidebg1" data-bgfit="cover" data-bgposition="center center"
                        data-bgrepeat="no-repeat">
                    <div class="tp-caption mediumlarge_light_white_center customin customout start" data-x="30"
                    data-hoffset="0" data-y="180" data-customin="x:0;y:-100;z:0;"
                    data-customout="x:0;y:0;z:0;" data-speed="1000" data-start="500"
                    data-easing="Back.easeInOut" data-endspeed="300">
                        {{$item->title}}
                    </div>
                    <div class="tp-caption   tp-resizeme" data-x="30"
                         data-hoffset="0" data-y="400" data-customin="x:0;y:-100;z:0;"
                         data-customout="x:0;y:0;z:0;" data-speed="1000" data-start="500"
                         data-easing="Back.easeInOut" data-endspeed="300">
                        <div class="consulting-rev-text" >
                            {!! $item->desc !!}
                        </div>
                    </div>
                </li>
            @endforeach

        </ul>
    </div>
</div>
