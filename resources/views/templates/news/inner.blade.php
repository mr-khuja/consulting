@extends('layouts.app')

@section('content')
    @if(!empty($new))
        <div style="height: 160px">

        </div>
        <div class="page_title"
             style="background-image: url(/images/newz.jpg) !important; background-repeat: no-repeat !important;">
            <div class="container">
                <div class="breadcrumbs">
                <span>
                    <a href="{{ route('home') }}" class="home">@lang('Home')</a>
                </span>
                <span><i class="fa fa-angle-right"></i></span>
                <span property="itemListElement" typeof="ListItem">
                    <a href="{{ route('news-page') }}"> @lang('News')</a>
                </span>
                <span>
                    <i class="fa fa-angle-right"></i>
                </span>
                    <span property="itemListElement" typeof="ListItem">
                    <span property="name">{{ $new->title }}</span>
                    <meta property="position" content="2">
                </span>
                </div>
                <h1 class="h2">{{ $new->title }}</h1>
            </div>
        </div>

        <div class="container">
            <div class="content-area">
                <article id="post-748"
                         class="post-748 post type-post status-publish format-standard has-post-thumbnail hentry category-funding-trends tag-401k tag-online-services tag-portfolios">
                    <div class="entry-content">
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1452687555475">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
                                <div class="vc_column-inner vc_custom_1452702342137">
                                    <div class="wpb_wrapper">
                                        <div class="vc_custom_heading no_stripe text_align_left">
                                            <h2 style="text-align: left">{{ $new->title }}</h2>
                                        </div>
                                        <div class="post_details_wr ">
                                            <div class="stm_post_info">
                                                <div class="stm_post_details clearfix">
                                                    <ul class="clearfix">
                                                        <li class="post_date">
                                                            <i class="fa fa fa-clock-o"></i>
                                                            {{ date('d.m.Y', strtotime($new->created_at) )  }}
                                                        </li>

                                                    </ul>
                                                </div>
                                                <div class="post_thumbnail">
                                                    <img src="{{ $new->image }}" alt="{{ $new->title }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element  vc_custom_1456126057085">
                                            <div class="wpb_wrapper">
                                                {!!  $new->full !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>


    @endif
@endsection
