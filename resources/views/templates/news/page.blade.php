@extends('layouts.app')

@section('content')
    <div style="height: 160px;"></div>
    <div class="page_title"
         style="background-image: url(/images/newz.jpg) !important; background-repeat: no-repeat !important;">
        <div class="container">
            <div class="breadcrumbs">
                <span>
                    <a href="{{ route('home') }}" class="home">@lang('Home')</a>
                </span>
                <span><i class="fa fa-angle-right"></i></span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name">@lang('News')</span>
                    <meta property="position" content="2">
                </span>
            </div>
            <h1 class="h2">@lang('News')</h1>
        </div>
    </div>
    @if(!empty($news))
        <div class="container">
            <div class="posts_grid">
                <ul class="post_list_ul">
                    @foreach($news as $new)
                        <li id="post-748"
                            class="post-748 post type-post status-publish format-standard has-post-thumbnail hentry category-funding-trends tag-401k tag-online-services tag-portfolios">
                            <div class="post_thumbnail"><a
                                    href="{{ route('news', $new->id) }}"><img
                                        width="100%"
                                        src="{{ $new->image }}"
                                        class="attachment-consulting-image-350x204-croped size-consulting-image-350x204-croped wp-post-image"
                                        alt=""></a>
                            </div>
                            <h5>
                                <a href="{{ route('news', $new->id) }}">{{ $new->title }}</a></h5>
                            <div class="post_date"><i class="fa fa-clock-o"></i> {{ date('d.m.Y', strtotime($new->created_at)) }}</div>
                        </li>
                    @endforeach
                </ul>
                {{$news->links()}}
            </div>
        </div>

    @endif
@endsection
