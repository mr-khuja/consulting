@extends('layouts.app')

@section('content')
    <div style="height: 160px"></div>
    @if(!empty($page))
    <div class="page_title"
         style="background-image: url({{$page->baner}}) !important; background-repeat: no-repeat !important;">
        <div class="container">
            <div class="breadcrumbs">
                <span>
                    <a href="{{ route('home') }}" class="home">@lang('Home')</a>
                </span>
                <span>
                    <i class="fa fa-angle-right"></i>
                </span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name">{{ $page->title }}</span>
                    <meta property="position" content="2">
                </span>
            </div>
            <h1 class="h2">{{ $page->title }}</h1>
        </div>
    </div>
    <div class="container">
        <div class="content-area">
            <article id="post-748" class="post-748 post type-post status-publish format-standard has-post-thumbnail hentry category-funding-trends tag-401k tag-online-services tag-portfolios">
                <div class="entry-content">
                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1452687555475">
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
                            <div class="vc_column-inner vc_custom_1452702342137">
                                <div class="wpb_wrapper">
{{--                                    <div class="vc_custom_heading no_stripe text_align_left">--}}
{{--                                        <h2 style="text-align: left"> {{ $page->title }} </h2>--}}
{{--                                    </div>--}}
{{--                                    @if(strlen($page->image) > 10)--}}
{{--                                        <div class="post_details_wr ">--}}
{{--                                            <div class="stm_post_info">--}}
{{--                                                <div class="post_thumbnail">--}}
{{--                                                    <img src="{{ $page->image }}" alt="{{ $page->title }}">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    @endif--}}
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1456126057085">
                                        <div class="wpb_wrapper">
                                            @if(!isset($ceo))
                                            {!!  $page->full !!}
                                            @endisset
                                            @isset($career)
                                                <div class="row career-row">
                                                    @foreach($career as $item)
                                                        <div class="col-md-6">
                                                            <a href="{{route('page', $item->id)}}">
                                                                <div class="career-card">
                                                                    <div class="col-xs-6 image">
                                                                        <img src="{{$item->image}}">
                                                                    </div>
                                                                    <div class="col-xs-6 text">
                                                                        <h4>{{$item->title}}</h4>
                                                                        {{$item->short}}
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endisset
                                            @isset($vacancies)
                                                @if(App()->getLocale() == 'ru')
                                                    <p>@lang('Вы можете найти свободные вакансии здесь')→ </p>
                                                @else
                                                    <p>@lang('You can search available vacancies here')→ </p>
                                                @endif
                                                <div class="table career-table">
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td><strong>№</strong></td>
                                                            <td><strong>@lang('Vacancy')</strong></td>
                                                            <td><strong>@lang('Team/Department')</strong></td>
                                                            <td><strong>@lang('Period of vacancy')</strong>
                                                            </td>
                                                        </tr>
                                                        @foreach($vacancies as $key => $vac)
                                                            <tr data-toggle="modal"
                                                                data-target="#career-modal-{{$vac->id}}">
                                                                <td>{{$key+1}}</td>
                                                                <td>{{$vac->title}}</td>
                                                                <td>{{$vac->team}}</td>
                                                                <td>{{$vac->period}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    </figure>
                                                </div>
                                                @if(App()->getLocale() == 'ru')
                                                    <h3>@lang('You can') <a href="/appform.doc">@lang('download the application form')</a>, @lang('fill out and send us to our postal address')</h3>
                                                @elseif(App()->getLocale() == 'en')
                                                    <h3>Вы можете скачать анкету <a href="/appform_ru.doc">здесь</a>, заполнить и отправить на адрес hr@gkd.uz</h3>
                                                @endif
                                            @endisset
                                            @isset($ceo)
                                                <div class="stm_testimonials cols_1 style_2">
                                                    <div class="item">
                                                        <div class="testimonial">
                                                            {!! $page->full !!}
                                                        </div>
                                                        <div class="testimonial-info clearfix">
                                                            <div class="testimonial-image"><img width="50" height="50" src="/images/ceo.jpg" class="attachment-consulting-image-50x50-croped size-consulting-image-50x50-croped wp-post-image" alt=""></div>
                                                            <div class="testimonial-text">
                                                                <div class="name">Yoon Yong Jin</div>
                                                                <div class="company">CEO</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .stm_testimonials .item .testimonial p, .stm_testimonials .item .testimonial p strong {
                                                        color: #777;
                                                        font-weight: normal;
                                                    }
                                                </style>
                                            @endisset
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
    @endif
@endsection
