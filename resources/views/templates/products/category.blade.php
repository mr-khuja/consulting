@extends('layouts.app')

@section('content')
    <div style="height: 160px;"></div>
    <div class="page_title"
         style="background-image: url(/images/products.jpg) !important; background-repeat: no-repeat !important;">
        <div class="container">
            <div class="breadcrumbs">
                <span>
                    <a href="{{ route('home') }}" class="home" style="color: #fff;">@lang('Home')</a>
                </span>
                <span>
                    <i class="fa fa-angle-right" style="color: #fff;"></i>
                </span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name" style="color: #fff;">@lang('Products')</span>
                    <meta property="position" content="2">
                </span>
            </div>
            <h1 class="h2" style="color: #fff;">@lang('Products')</h1>
        </div>
    </div>
@endsection
