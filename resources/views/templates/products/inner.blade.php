@extends('layouts.app')
@section('content')
    <div style="height: 160px"></div>
    @if(!empty($product))
        <div class="page_title"
             style="background-image: url({{$product->baner}}) !important; background-repeat: no-repeat !important;">
            <div class="container">
                <div class="breadcrumbs">
                <span>
                    <a href="{{ route('home') }}" class="home">@lang('Home')</a>
                </span>
                    <span>
                    <i class="fa fa-angle-right"></i>
                </span>
                    <span property="itemListElement" typeof="ListItem">
                    <span property="name">{{ $product->title }}</span>
                    <meta property="position" content="2">
                </span>
                </div>
                <h1 class="h2">{{ $product->title }}</h1>
            </div>
        </div>

        <div class="container">
            <div class="content-area">
                <article id="post-609" class="post-609 stm_works type-stm_works status-publish has-post-thumbnail hentry stm_works_category-surface-transport-logistics">
                    <div class="entry-content">
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1452687555475">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="vc_custom_heading vc_custom_1453269716083 text_align_left has_subtitle">
                                            <h2> {{ $product->title }}</h2>
                                        </div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1453271494452">
                                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    <img class="vc_single_image-img " src="{{ $product->image }}" alt="{{ $product->title }}">
                                                                </div>
                                                            </figure>
                                                        </div>
                                                        <div class="stm-spacing" style="height: 0px;" id="stm-spacing-5cd959795ab61">

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div class="vc_custom_heading no_stripe remove_padding vc_custom_1456310480533 text_align_left">
                                                            <h6 style="line-height: 26px;text-align: left">
                                                                {!! $product->full !!}
                                                            </h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>


    @endif


@endsection
