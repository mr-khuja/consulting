@extends('layouts.app')

@section('content')
    <div style="height: 160px;"></div>
    <div class="page_title"
         style="background-image: url(/images/products.jpg) !important; background-repeat: no-repeat !important;">
        <div class="container">
            <div class="breadcrumbs">
                <span>
                    <a href="{{ route('home') }}" class="home" style="color: #fff;">@lang('Home')</a>
                </span>
                <span>
                    <i class="fa fa-angle-right" style="color: #fff;"></i>
                </span>
                <span property="itemListElement" typeof="ListItem">
                    <span property="name" style="color: #fff;">@lang('Products')</span>
                    <meta property="position" content="2">
                </span>
            </div>
            <h1 class="h2" style="color: #fff;">@lang('Products')</h1>
        </div>
    </div>
    <div class="container">

        <div class="content-area">

            <article id="post-558" class="post-558 page type-page status-publish hentry">

                <div class="entry-content">
                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1452687555475">
                        @if(!empty($categories))
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1452687555475">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
                                    <div
                                        class="vc_column-inner stm_services_tabs  ui-tabs ui-widget ui-widget-content ui-corner-all">
                                        <div class="services_categories">
                                            <ul class="clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all"
                                                role="tablist">
                                                @foreach($categories as $item)
                                                    <li class="ui-state-default ui-corner-top"
                                                        role="tab" tabindex="0" aria-controls="service-tab-audit"
                                                        aria-labelledby="ui-id-{{$item->id}}" aria-selected="true"
                                                        aria-expanded="true">
                                                        <a href="#tab-{{$item->id}}" class="ui-tabs-anchor"
                                                           role="presentation" tabindex="-1"
                                                           id="ui-id-{{$item->id}}">{{$item->title}}</a>
                                                    </li>
                                                @endforeach
                                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1"
                                                    aria-controls="service-tab-international-business"
                                                    aria-labelledby="ui-id" aria-selected="false"
                                                    aria-expanded="false">
                                                    <a href="{{route('page', 25)}}" class="ui-tabs-anchor"
                                                       role="presentation" tabindex="-1"
                                                       id="ui-id">@lang('What is made')</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="wpb_wrapper">
                                            <div class="stm_services style_1 cols_3">
                                                @foreach($categories as $item)
                                                    <div
                                                        class="services_tabs ui-tabs-panel ui-widget-content ui-corner-bottom"
                                                        id="tab-{{$item->id}}" aria-labelledby="ui-id-{{$item->id}}"
                                                        role="tabpanel" aria-hidden="false">
                                                        <div class="wpb_wrapper">
                                                            <div class="wpb_text_column wpb_content_element  vc_custom_1456122392413">
                                                                <div class="wpb_wrapper">
                                                                    <h2>{{$item->title}}</h2>
                                                                    <p style="font-size: 18px;">{{$item->short}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @foreach($products as $product)
                                                            @if($product->category_id == $item->category_id || $product->category_id == $item->id)
                                                                <div class="item">
                                                                    <div class="item_wr">

                                                                        <div class="item_thumbnail">
                                                                            <a href="{{ route('product', $product->id) }}">
                                                                                <img width="100%"
                                                                                     src="{{ $product->image }}"
                                                                                     class="attachment-consulting-image-255x182-croped"
                                                                                     alt=""
                                                                                     srcset="{{ $product->image }}">
                                                                            </a>
                                                                        </div>
                                                                        <div class="content">
                                                                            <h5>
                                                                                <a href="{{ route('product', $product->id) }}">{{ $product->title }}</a>
                                                                            </h5>
                                                                            <p>{{ $product->short }}</p>
                                                                            <a class="read_more"
                                                                               href="{{ route('product', $product->id) }}">
                                                                                <span>@lang('Read more')</span>
                                                                                <i class=" fa fa-chevron-right stm_icon"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            jQuery(document).ready(function ($) {
                                                "use strict";
                                                jQuery(".stm_services_tabs").tabs({
                                                    hide: 'fadeOut',
                                                    show: 'fadeIn',
                                                    duration: 'fast'
                                                });
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

            </article>
        </div>

    </div>
@endsection
