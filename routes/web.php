<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('setlocale/{locale}', function ($locale) {

    if (in_array($locale, \Config::get('app.locales'))) {

        Session::put('locale', $locale);

    }

    return redirect()->back();

})->name('lang');

Auth::routes();

Route::get('/', 'HomeController@home')->name('home');
//Route::get('/', function(){
//    dd(Session::get('locale'));
//})->name('home');
Route::get('page/{id}', 'HomeController@page')->name('page');
Route::get('news-page', 'HomeController@NewsPage')->name('news-page');
Route::get('news/{id}', 'HomeController@news')->name('news');
Route::get('awards', 'HomeController@awards')->name('awards');
Route::post('contact', 'HomeController@contact')->name('contact');
Route::get('products-page', 'HomeController@ProductsPage')->name('products-page');
Route::get('products/{id}', 'HomeController@ProductInner')->name('product');



Route::group([
    'prefix' => 'req',
    'middleware' => ['auth']
], function() {

    Route::group(['prefix' => 'pages'], function() {
        Route::match(['post', 'get'], '/', 'Admin\PagesController@list');
        Route::post('c', 'Admin\PagesController@create');
        Route::match(['post', 'get'], 'e/{id}', 'Admin\PagesController@edit');
        Route::match(['post', 'get'], 't/{id}/{lang}', 'Admin\PagesController@translate');
        Route::get('d/{id}', 'Admin\PagesController@delete');
        Route::post('u', 'Admin\PagesController@upload');
    });

    Route::group(['prefix' => 'vacancies'], function() {
        Route::match(['post', 'get'], '/', 'Admin\VacanciesController@list');
        Route::post('c', 'Admin\VacanciesController@create');
        Route::match(['post', 'get'], 'e/{id}', 'Admin\VacanciesController@edit');
        Route::match(['post', 'get'], 't/{id}/{lang}', 'Admin\VacanciesController@translate');
        Route::get('d/{id}', 'Admin\VacanciesController@delete');
        Route::post('u', 'Admin\VacanciesController@upload');
    });

    Route::group(['prefix' => 'news'], function() {
        Route::match(['post', 'get'], '/', 'Admin\NewsController@list');
        Route::post('c', 'Admin\NewsController@create');
        Route::match(['post', 'get'], 'e/{id}', 'Admin\NewsController@edit');
        Route::match(['post', 'get'], 't/{id}/{lang}', 'Admin\NewsController@translate');
        Route::get('d/{id}', 'Admin\NewsController@delete');
        Route::post('u', 'Admin\NewsController@upload');
    });

    Route::group(['prefix' => 'products'], function() {
        Route::match(['post', 'get'], '/', 'Admin\ProductsController@list');
        Route::post('c', 'Admin\ProductsController@create');
        Route::match(['post', 'get'], 'e/{id}', 'Admin\ProductsController@edit');
        Route::match(['post', 'get'], 't/{id}/{lang}', 'Admin\ProductsController@translate');
        Route::get('d/{id}', 'Admin\ProductsController@delete');
        Route::post('u', 'Admin\ProductsController@upload');
        Route::post('o', 'Admin\ProductsController@order');
    });

    Route::group(['prefix' => 'feedbacks'], function() {
        Route::match(['post', 'get'], '/', 'Admin\FeedbacksController@list');
        Route::post('c', 'Admin\FeedbacksController@create');
        Route::match(['post', 'get'], 'e/{id}', 'Admin\FeedbacksController@edit');
        Route::match(['post', 'get'], 't/{id}/{lang}', 'Admin\FeedbacksController@translate');
        Route::get('d/{id}', 'Admin\FeedbacksController@delete');
        Route::post('u', 'Admin\FeedbacksController@upload');
    });

    Route::group(['prefix' => 'slides'], function() {
        Route::match(['post', 'get'], '/', 'Admin\SlidesController@list');
        Route::post('c', 'Admin\SlidesController@create');
        Route::match(['post', 'get'], 'e/{id}', 'Admin\SlidesController@edit');
        Route::match(['post', 'get'], 't/{id}/{lang}', 'Admin\SlidesController@translate');
        Route::get('d/{id}', 'Admin\SlidesController@delete');
        Route::post('u', 'Admin\SlidesController@upload');
    });

    Route::group(['prefix' => 'categories'], function() {
        Route::match(['post', 'get'], '/', 'Admin\CategoriesController@list');
        Route::post('c', 'Admin\CategoriesController@create');
        Route::match(['post', 'get'], 'e/{id}', 'Admin\CategoriesController@edit');
        Route::match(['post', 'get'], 't/{id}/{lang}', 'Admin\CategoriesController@translate');
        Route::get('d/{id}', 'Admin\CategoriesController@delete');
        Route::post('u', 'Admin\CategoriesController@upload');
    });

    Route::group(['prefix' => 'awards'], function() {
        Route::match(['post', 'get'], '/', 'Admin\AwardsController@list');
        Route::post('c', 'Admin\AwardsController@create');
        Route::match(['post', 'get'], 'e/{id}', 'Admin\AwardsController@edit');
        Route::get('d/{id}', 'Admin\AwardsController@delete');
        Route::post('u', 'Admin\AwardsController@upload');
        Route::post('o', 'Admin\AwardsController@order');
    });

    Route::group(['prefix' => 'partners'], function() {
        Route::match(['post', 'get'], '/', 'Admin\PartnersController@list');
        Route::post('c', 'Admin\PartnersController@create');
        Route::match(['post', 'get'], 'e/{id}', 'Admin\PartnersController@edit');
        Route::get('d/{id}', 'Admin\PartnersController@delete');
        Route::post('u', 'Admin\PartnersController@upload');
        Route::post('o', 'Admin\PartnersController@order');
    });

    Route::group(['prefix' => 'menu'], function() {
        Route::match(['post', 'get'], '/', 'Admin\MenuController@list');
        Route::post('c', 'Admin\MenuController@create');
        Route::match(['post', 'get'], 'e/{id}', 'Admin\MenuController@edit');
        Route::match(['post', 'get'], 't/{id}/{lang}', 'Admin\MenuController@translate');
        Route::get('d/{id}', 'Admin\MenuController@delete');
        Route::post('o', 'Admin\MenuController@order');
        Route::post('w', 'Admin\MenuController@weight');
    });

    Route::match(['post', 'get'], 'settings', 'Admin\SettingsController@settings');
    Route::post('upload', 'Admin\SettingsController@upload');
    Route::match(['post', 'get'], 'profile', 'Admin\SettingsController@profile');
    Route::match(['post', 'get'], 'social', 'Admin\SettingsController@social');
    Route::get('logout', 'Admin\SettingsController@logout')->name('user_logout');
    Route::get('counts', 'Admin\SettingsController@counts')->name('counts');
    Route::get('latest', 'Admin\SettingsController@latest')->name('latest');
});


Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth']
//    'middleware' => 'auth'
], function() {
    Route::get('{any}', function () {
        return view('admin.home');
    })->where('any','.*');
    Route::get('/', function () {
        return view('admin.home');
    });
});
